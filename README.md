# APS monitor and control
This project will contain all the necessary code to monitor and control the devices on an APS  in an easily configurable fashion and then transmit it through the OPC ua protocol.
It is currently a work in progress.

## MCU_Device_manager
This is the device manager implemented on the micro controller. It contains a configuration for each I2C unit. 
It contains the managing parts and structure and it contains the implementations for several sensors.

## MCU_OPCua_I2C_example
This folder contains a micro controller project with an OPC ua server using the Open62541 stack. It starts the LWIP networking stack, starts the server and continuously reads out a I2C sensor and updates a variable value. This variable can be read by a client. The client is also included as a python script.
It connects to the server and reads out the variable containing the sensor data. It then displays this value on a live graph.

## python_mini_M&C
This folder contains a miniature python test server. It contains both a real I2C sensor reader for the raspberry pi and a fake that can be run from any system. 
The other part is the OPC ua server. It contains the sensor data as nodes and variables and can provide this access through regular data requests and by subscribing to events.