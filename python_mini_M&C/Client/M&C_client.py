import sys
sys.path.insert(0, "..")
import time
from opcua import Client

client = Client("opc.tcp://0.0.0.0:4840/freeopcua/server/")


class SubHandler(object):

    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another
    thread if you need to do such a thing
    """
    def event_notification(self, event):
        print("New event received: ", event)
        # print(dir(event)) see all attributes/methods/fields/etc this object has


try:
    client.connect()

    # the server should contain all the devices in the  "Devices" node, an object in the "baseObjectNode"
    # Basically navigate to Root/baseObjectNode/devices/{actualDevices}

    root = client.get_root_node()
    print("Root node is: ", root)
    print("he goes by the name: ", root.get_browse_name())

    # go to the "baseObjectNode" Root/baseObjectNode
    object_node = client.get_objects_node()

    # see what children the baseObjectNode has (should contain "devices" node)
    objects = object_node.get_children()
    print("the following nodes are in the \"baseObjectNode", objects) # print the children


    print("looking if one of them is the \"devices\" node")

    devices = []  # for containing the actual devices in the "devices"node

    # go through the children and see if one of them is the "devices"node
    for i in range(len(objects)):
        if "devices" in str(objects[i].get_browse_name()):
            print("found it!")
            devices = objects[i].get_children() # store all devices in the "devices" node

    nof_devices = len(devices)  # store the number of devices
    print("there are", nof_devices, "devices")

    # see what devices the "devices" node has
    print( "The server has the following devices:")
    for i in range(nof_devices):
        print("\t", devices[i], devices[i].get_browse_name())

    # this shows how to find a node if its name and location are already known
    sensor_update_event = root.get_child(["0:Types", "0:EventTypes", "0:BaseEventType", "2:Sensor_update"])
    sensor_update_object = root.get_child(["0:Objects", "2:Sensor_update"])

    # subscribe to the event, checks every 1000ms for new updates
    # the function SubHandler() handles the event
    msclt = SubHandler()
    sub = client.create_subscription(1000, msclt)
    handle = sub.subscribe_events(sensor_update_object, sensor_update_event)

    # this code gets the method object
    toggle_method = root.get_child(["0:Objects", "2:toggle"])
    toggle_method_alt = root.get_child(["0:Objects", "2:toggle_alt"])

    writables = root.get_child(["0:Objects", "2:writables"])
    writable_0 = writables.get_child(["2:writable_0"])
    result_0 = writables.get_child(["2:writable_0_result"])
    toggle = True

    print(writable_0.get_value())
    print(result_0.get_value())

    while 1:
        # print all the values by individually requesting all sensor-variables
        # this is very inefficient, a new request is send for each get_value call
        # Better to contain them in an array https://github.com/FreeOpcUa/python-opcua/issues/604#issuecomment-393308710
        print("=======================================================")
        print("requesting all variables")
        for i in range(nof_devices):
            sensor_variables = devices[i].get_children()
            print("device: ", devices[i].get_browse_name(), "has the following sensor data:")

            for j in range(len(sensor_variables)):
                print("\t", sensor_variables[j].get_browse_name(), ":", sensor_variables[j].get_value())

        # call the function to turn off a device
        result = toggle_method.call_method("2:toggle_device", False)
        print(result)

        # alternative method, using different argument formatting styles
        result = toggle_method_alt.call_method("2:toggle_device_alt", False)
        print(result)

        time.sleep(10)

        # call the function to turn on a device
        result = toggle_method.call_method("2:toggle_device", True)
        print(result)

        # alternative method, using different argument formatting styles
        result = toggle_method_alt.call_method("2:toggle_device_alt", True)
        print(result)

        writable_0.set_value(toggle)
        if toggle:
            toggle = False
        else:
            toggle = True

        time.sleep(10)

        print("result: ", result_0.get_value())


finally:
    client.disconnect()
