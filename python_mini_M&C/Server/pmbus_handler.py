"""
this file is capable of sending commands to pmbus devices connected to it.
it can create 'device' objects and 'sensor' objects and use the function ...
get_measurement() to read the device
one of the functions (literal, direct, vout_mode) gets bound to a specific sensor type
and is called using format() to format the data received by the get_measurement() function
"""
import smbus
import time

bus = smbus.SMBus(1)  # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)


class pmbus_device:
    def __init__(self, device_name, device_address):
        self.name = device_name
        self.address = device_address

    def enable_on_off(self, on_off_bool):  # enables/disables the the usage of the on/off (operations)command
        if on_off_bool:
            write_data = 0x18  # enables on/off command, see PMBUS specs section 12.2 for details
        else:
            write_data = 0x00

        try:
            bus.write_byte_data(self.address, 0x02, write_data)
        except IOError:
            return None

    def device_on_off(self, on_off_bool):
        if on_off_bool:
            write_data = 0x80  # enabless the device
        else:
            write_data = 0x00  # disables the device immediatly (quick discharge)

        try:
            bus.write_byte_data(self.address, 0x01, write_data)
        except IOError:
            return None


class pmbus_sensor:

    def __init__(self, CMD_name, CMD_ADDR, CMD_FORMAT):
        self.name = CMD_name
        self.address = CMD_ADDR
        self.data_format = CMD_FORMAT

        # bind the object to the correct data formatting function during initialisation
        # can be called with name: "format" obj.format(raw)
        if CMD_FORMAT == "literal":
            setattr(self, "format_data", self.literal)
        if CMD_FORMAT == "direct":
            setattr(self, "format_data", self.direct)
        if CMD_FORMAT == "vout_mode":
            setattr(self, "format_data", self.vout_mode)

    def literal(self, raw_word):

        mantissa = raw_word & 0b0000011111111111  # mask upper 5 bits to 0
        exponent = raw_word >> 11  # shift exponent to the right

        if exponent & 0b10000 > 1:  # if the exponent is negative (MSB == 1)
            exponent = exponent & 0b01111
            exponent = exponent - 16

        if mantissa & 0b10000000000 > 1:  # if the mantissa is negative (MSB == 1)
            mantissa = mantissa & 0b01111111111
            mantissa = mantissa - 1024

        return mantissa * pow(2, exponent)  # temperature = mantissa*2^exponent

    def direct(self, raw_word):  # TODO: this function (if implemented in any of the device)
        return -1

    def vout_mode(self, raw_word, raw_byte):
        # note: raw_byte and raw_word are formally called: VOUT_MODE and VOUT_COMMAND

        exponent = raw_byte & 0b00011111  # 5 lowest bits of the raw_byte
        mantissa = raw_word		# this is a 16 bit unsigned integer

        # check if the exponent is negative
        if exponent & 0b10000 >= 1:  # if the exponent is negative (MSB == 1)
            exponent = exponent & 0b01111 	# set sign bit to 0
            exponent = exponent - 16 	# two's complement

        return mantissa * pow(2, exponent)  # measurement = mantissa*2^exponent

    def get_measurement(self, device_address):
        # reads the device
        try:
            raw_word = bus.read_word_data(device_address, self.address)
        except IOError:
            return None

        if self.data_format == "vout_mode":		# requires two commands
            time.sleep(0.02) 	# ensures the delay between commands is large enough
            try:
                raw_byte = bus.read_byte_data(device_address, 0x20)
            except IOError:
                return None
            measurement = self.format_data(raw_word, raw_byte)
        else:
            measurement = self.format_data(raw_word)

        time.sleep(0.02) 	# ensures the delay between commands is large enough
        return measurement

    def format_data(self, raw_word):
        pass


