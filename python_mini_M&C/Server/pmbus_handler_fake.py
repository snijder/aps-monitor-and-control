"""
this file exists to simulate the pmbus devices when they aren't connected
it is a simple way to get around having to connect the devices to the pmbus
of a raspberry pi or other linux machine

if you want to change to the real pmbus reader, change which library gets imported in PMBUS_instantiator.py
"""


import time
from random import *


class pmbus_device:
    def __init__(self, device_name, device_address):
        self.name = device_name
        self.address = device_address

    def enable_on_off(self, on_off_bool):  # enables/disables the the usage of the on/off (operations)command
        if on_off_bool:
            write_data = 0x18  # enables on/off command, see PMBUS specs section 12.2 for details
        else:
            write_data = 0x00

        try:
            if on_off_bool:
                print(self.name, "on/off commands have been enabled")
            else:
                print(self.name, "on/off commands have been disabled")

        except IOError:
            return None

    def device_on_off(self, on_off_bool):
        if on_off_bool:
            write_data = 0x80  # enabless the device
        else:
            write_data = 0x00  # disables the device immediatly (quick discharge)

        try:
            if on_off_bool:
                print(self.name, "has been turned on")
            else:
                print(self.name, "has been turned off")
        except IOError:
            return None


# replaces actually reading the bus since that requires linux and the devices connected
class pmbus_sensor:
    def __init__(self, CMD_name, CMD_ADDR, CMD_FORMAT):
        self.name = CMD_name
        self.address = CMD_ADDR
        self.data_format = CMD_FORMAT

    def get_measurement(self, a):
        return random()


