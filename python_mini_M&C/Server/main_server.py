"""
This code is an example/mock-up application, meant to show how the final application will work.
This is the OPC ua server. It is capable of reading PMbus/I2C devices (or fake reading them)
and making that data available to clients that can connect to it, or allow clients to
subsribe to its regularly generated sensor readout updates.

in main_server:
	creates server
	navigates around address space in various ways
	creates an event, and adds all sensors to this as property
	has a function that does absolutely nothing except printing a message saying it did something
	adds every device as object and every sensor as variable (are individually readable by clients)
	in while loop:
		updates all device readouts
		updates the opc ua variables with the readout
		updates the opc ua event properties with the readout
		pushes an event to subscribed clients containing the new sensor readouts
"""

from opcua import ua, uamethod, Server
import time
import PMBUS_instantiator as pmbus

import sys
sys.path.insert(0, "..")


# this function gets called by a client
def toggle_device(parent, toggle):
	string = ""
	if toggle.Value:
		string = ("turning on the" + pmbus.devices[1].name)
	if not toggle.Value:
		string = ("turning off the" + pmbus.devices[1].name)

	print(string)
	pmbus.devices[1].device_on_off(toggle.Value)

	return [ua.Variant(string, ua.VariantType.String)]

# holds the device as object with the sensors as variables in OPC ua address space
# this does not contain the actual values, just the address space objects and variables
class device_obj:
	def __init__(self, device):
		self.ua_object = device
		self.ua_variable_readout = []


# setup our server
server = Server()
server.set_endpoint("opc.tcp://0.0.0.0:4840/freeopcua/server/")

# setup our own namespace, not really necessary but should as spec
uri = "http://LOFAR2MC.opcua.ASTRON.io"
idx = server.register_namespace(uri)

# get Objects node, this is where we should put our nodes
objectsNode = server.get_objects_node()
devicesNode = objectsNode.add_object(idx, "devices")

# list for containing all our device objects
ua_devices = [] # list of objects containing the

# create a custom event. step 1 by adding an object and eventType, (both named sensor_update in address space)
sensor_update_object = objectsNode.add_object(idx, "Sensor_update")
sensor_update_event = server.nodes.base_event_type.add_object_type(idx, 'Sensor_update')

# alternative way to create in/out arguments
inarg = ua.Argument()
inarg.Name = "toggle_bool"
inarg.DataType = ua.NodeId(ua.ObjectIds.Boolean)
inarg.ValueRank = -1
inarg.ArrayDimensions = []
inarg.Description = ua.LocalizedText("turn on or of one of the modules")
outarg = ua.Argument()
outarg.Name = "Result"
outarg.DataType = ua.NodeId(ua.ObjectIds.String)
outarg.ValueRank = -1
outarg.ArrayDimensions = []
outarg.Description = ua.LocalizedText("resultant message")


# create a method that is callable by a client
method_object = objectsNode.add_object(idx, "toggle")
example_method = method_object.add_method(idx, "toggle_device", toggle_device, [ua.VariantType.Boolean], [ua.VariantType.String])
alt_method_object = objectsNode.add_object(idx, "toggle_alt")
alt_method = alt_method_object.add_method(idx, "toggle_device_alt", toggle_device, [inarg], [outarg])

# create a node for writable (by the client) variables
writables = objectsNode.add_object(idx, "writables")
writable_0 = writables.add_variable(idx, "writable_0", True, ua.VariantType.Boolean)
result_0 = writables.add_variable(idx, "writable_0_result", "initial text", ua.VariantType.String)

writable_0.set_writable()


# create an OPC ua object for each device
for i in range(len(pmbus.devices)):

	# add object to the "device" node in OPCua address space
	ua_devices.append(device_obj(devicesNode.add_object(idx, pmbus.readout_buffer[i].name)))

	# add all the sensors as 'device'variables and event properties
	for j in range(len(pmbus.sensors)):
		# add all the individual variables (this is inefficient, every variable requires a seperate request)
		# this is here so it still is possible to call any specific variable
		ua_devices[i].ua_variable_readout.append(ua_devices[i].ua_object.add_variable(idx, pmbus.sensors[j].name, 0.0, ua.VariantType.Float))

		# add all sensors as properties to the event (note: 2nd arg generates a variable name from a string)
		event_variable_name = pmbus.readout_buffer[i].name + str(j)
		sensor_update_event.add_property(idx, event_variable_name, ua.Variant(0, ua.VariantType.Float))

# create the actual event
sensor_update = server.get_event_generator(sensor_update_event, sensor_update_object)

# guess what this does?
server.start()

try:

	on_off_counter = 0
	on_off = True #start with the devices on

	# this enables the usage of on/off commands on the devices
	pmbus.devices[0].enable_on_off(True)
	pmbus.devices[1].enable_on_off(True)

	temp_old = writable_0.get_value()
	temp = writable_0.get_value()

	while 1:

		# update all sensor values by reading and then storing
		pmbus.update_readout()

		# update all the OPC ua device object variables with the new readout values
		for i in range(len(ua_devices)):  # go through all devices
			for j in range(len(pmbus.sensors)):  # go through all sensors in a device
				# update the opc ua variables with the values from the readout buffer
				ua_devices[i].ua_variable_readout[j].set_value(pmbus.readout_buffer[i].sensor[j])  # update the single-item variables

				# update all the event variables (events send all properties at once, less messages)
				# event_variable_name is a variable name generated from a string, this was the only way i could find ...
				# ... to write code that can handle variable variable names.
				event_variable_name = str(pmbus.readout_buffer[i].name) + str(j)
				abomination = "sensor_update.event.%s = pmbus.readout_buffer[i].sensor[j]" % event_variable_name
				exec(abomination)
				# sorry for this abomination

			# this handles turning on/off device 0
			on_off_counter = on_off_counter + 1
			if on_off_counter >= 60:
				on_off_counter = 0

				# since we only have 2 devices
				pmbus.devices[0].device_on_off(on_off)
				on_off = not on_off


		# trigger an event
		sensor_update.trigger()  # push the event to the subscribed clients
		print("triggering event")

		time.sleep(1)

		temp = writable_0.get_value()
		print(temp_old, "  ||  ", temp)
		################# call functions through (client) writable variables
		if temp_old != temp:	 # if the value changed
			if not temp:
				pmbus.devices[1].device_on_off(False)
				result_0.set_value("turning off the " + pmbus.devices[1].name)
			else:
				pmbus.devices[1].device_on_off(True)
				result_0.set_value("turning on the " + pmbus.devices[1].name)
			temp_old = temp


finally:
	server.stop()

