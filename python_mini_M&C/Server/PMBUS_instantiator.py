"""
This code is an example/mock-up application, meant to show how the final application will work.
This is the OPC ua server. It is capable of reading PMbus/I2C devices (or fake reading them)
and making that data available to clients that can connect to it, or allow clients to
subsribe to its regularly generated sensor readout updates.

in pmbus_instantiator
    this file can use the library: pmbus_handler or pmbus_handler_fake
    the fake libray simulates the actual pmbus/i2C reading, as this requires 1: a linux device ...
    And 2: the devices to actually be connected to the bus.
    this is a simple way to get around that if only the OPC ua part is of interest
    When reading the sensors using the fake library, a random value between 0 and 1 is returned.

    creates an object for every device containing name and address
    creates an object for every sensor, containing name, command and data-format
    organises all devices and sensors in a list
    creates a buffer for containing all sensor readout values
    organised like this: buffer = [ [device0_sensor0, device0_sensor1, ...], [device1_sensor0, device1_sensor1, ...], ...
    a function to update all sensor values the server has.

    note: All devices are treated the same.  It currently is not possible to have one device measure something else.
    To do that, the loop in this code and in the update_readout() function will need to be changed.
"""

#from pmbus_handler import *       # real library that reads the pmbus devices
from pmbus_handler_fake import *    # fake library for when the pmbus devices aren't connected

# gets used in a list of these objects for containing all sensor values
class device_readout:
    def __init__(self, device_name):
        self.name = device_name
        self.sensor = []

# all pmbus devices and commands can be added below.

# instantiate all devices       # device object = pmbus_device(device_name, device_address)
BMR462 = pmbus_device("BMR462_node_0", 0x65)
BMR464 = pmbus_device("BMR464_node_0", 0x4f)

# set-up pmbus commands/sensors          # pmbus_sensor = pmbus_sensor(name, address, data_format)
temperature_sensor = pmbus_sensor("temperature", 0x8D, "literal")
input_voltage_sensor = pmbus_sensor("input_voltage", 0x88, "literal")
Output_voltage_sensor = pmbus_sensor("output_voltage", 0x8B, "vout_mode")
Output_current_sensor = pmbus_sensor("output_current", 0x8C, "literal")

# organise devices in list
devices = [BMR462, BMR464]

# organise all sensors in list
sensors = [temperature_sensor, input_voltage_sensor, Output_voltage_sensor, Output_current_sensor]

# create a buffer for storing all sensor readouts
readout_buffer = []

# create an object for every device, containing a name and an array of sensors
for i in range(len(devices)):  # go through all devices
    readout_buffer.append(device_readout(devices[i].name)) # add the device names
    for j in range(len(sensors)):  # add a place for every sensor
        readout_buffer[i].sensor.append(0)


# reads all devices and sensors and updates the readout_buffer
def update_readout():
    for i in range(len(devices)):  # go through all devices
        print("===== ", devices[i].name, " ========================")
        for j in range(len(sensors)):  # go through all sensors in a device
            # get the sensor readout and store it in a buffer for all sensor readouts
            readout_buffer[i].sensor[j] = sensors[j].get_measurement(devices[i].address)  # get the sensor value
            print(sensors[j].name, ": ", readout_buffer[i].sensor[j])
