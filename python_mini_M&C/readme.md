# Python mini server
This is a python based monitor and control application. the code is made up of 2 parts: the OPC ua server and client and the I2C manager.

* To use the OPC ua server, the FreeOPCua package needs to be isntalled https://github.com/FreeOpcUa/
* to use the I2C devices, a raspbarry pi is required as well as the smbus python package. (otherwise use the fake I2C manager)

# I2C manager
The I2C manager, currently instantiates 2 PMBUS devices. Those devices are periodically read through I2C, formatted and then have their formatted readouts stored in a buffer. There is also a fake variant of this file, useful for when the devices are not available to read out. This file can be enabled by uncommenting it in PMBUS_instantiator.py. 

## OPC ua server and client
The OPC ua server, copies the values in the databuffer to its objects (all the devices and sensors). If a client wants to read out an object, it will receive the latest sensor or device readout. The server also provides a subsription to the device readout. Any clients subsribed will receive the sensor readout at a regular interval. Finally, the server also has a function callable from the client that can turn on or the I2C devices. Currently only one device is turned on or off through this function. 
