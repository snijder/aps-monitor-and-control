/*
 * node.cpp
 *
 *  Created on: 27 Mar 2020
 *      Author: snijd
 */




//#include <iostream>

#include <string>
#include <cmath>

//#include "fsl_common.h"
//#include "peripherals.h"

#include <fsl_device_registers.h>
#include "fsl_debug_console.h"

//#include "fsl_lpi2c.h"
//#include "fsl_lpi2c_freertos.h"

extern "C" {
	#include <common.h>
}

#include "node.h"
#include "devices.h"



/*		node class implementation
 * 	==========================================
 * */
node::node(	monitored_device 	**monitored_ptr_arr, 	uint8_t _nof_monitored,	// **monitored_ptr_arr: pointer to pointer array of all monitored devices,
			node 				**subnodes_ptr_arr, 	uint8_t _nof_subnodes,	// **subnodes_ptr_arr: pointer to pointer array of all subnodes
			device 				**devices_ptr_arr, 		uint8_t _nof_devices,	// **devices_ptr_arr: pointer to pointer array of all non-monitored devices
			string 				_name,
			int8_t 				_switch_channel, 		int8_t _switch_nr){		//, I2C_switch *_switcher = nullptr)	// data about how to reach this node and where it is located

	name = _name;

	switch_nr = _switch_nr;				//to which switch this node is connected to (NONE means previous node doesn't have a switch)
	switch_channel	= _switch_channel;	//to which channel of a switch this node is connected to

	nof_monitored = _nof_monitored;
	monitored = monitored_ptr_arr;

	nof_subnodes = _nof_subnodes;
	subnodes = subnodes_ptr_arr;

	nof_devices = _nof_devices;
	devices = devices_ptr_arr;

	//check if any of the devices are an I2C switch, if so, set the switch pointer to it(only one switch allowed currently)
	for (int i = 0; i < _nof_devices; i++) {
		if (devices_ptr_arr[i]->type == I2C_switch_device) {
			switcher = reinterpret_cast<I2C_switch*>(devices_ptr_arr[i]);
			break;
		}
	}
}


uint16_t node::read_node(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &cnt, float *buffer) { //read all the devices on this node and its subnodes
	status_t status = -1;		//holds the status of the I2C read
	uint16_t failure_cnt = 0;	//whenever setting a switch fails, this counter is incremented


#if LOGGING_GENERAL == true
	PRINTF( "entering node: %s\r\n", name.c_str());
#endif

	for (int i = 0; i < nof_monitored; i++) { //go through all devices on this node
		failure_cnt+= monitored[i]->device_readout(I2C_Handle, cnt, buffer);
	}

	for (int i = 0; i < nof_subnodes; i++) {	//go through the subnodes and read all their devices and then subnodes
		if (switch_nr != NONE && switcher != nullptr) {				// if this node contains a switch

#if LOGGING_GENERAL == true
			PRINTF("%s - switching to channel: %d", subnodes[i]->name.c_str(), int(subnodes[i]->switch_channel));
			PRINTF( ", of switch: %d, %s located on: %s\r\n", int(switch_nr), switcher->name.c_str(), name.c_str());
#endif

			status = switcher->set_channel(I2C_Handle, subnodes[i]->switch_channel);	//change the selected channel to that of the next sub node


#if IGNORE_SWITCH_ERROR == false	//if true, skips nodes that become unreachable whenever setting the switch fails
			if(status == kStatus_Success ){
				failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
			}
			else{		//if the switching failed, abort reading the subnodes, as this will likely result in faulty data
				//TODO: for now only skips all devices (keeping old values), it would be useful to track over time

				cnt+= subnodes[i]->get_nof_datapoints();	//skip this many datapoints

				failure_cnt++;
			}
#else
			if(status != kStatus_Success ){
				PRINTF("%s: \t", name.c_str());
				failure_cnt ++;
			}
			failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
#endif
		}
		else {	//if there is no switch to worry about
			failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
		}



	}
	if (switch_nr != NONE && switcher != nullptr) {
		switcher->set_off(I2C_Handle); //turn of this switch when done
	}
	//PRINTF("failures: %d\r\n", failure_cnt);
	return failure_cnt;
}

uint16_t node::get_nof_datapoints() {	//counts the total number of datapoints this nodes (its devices and subnodes) has
	uint16_t nof_datapoints = 0;

	//get the number of data points the subnodes contain
	for (int i = 0; i < nof_subnodes; i++) {
		nof_datapoints = nof_datapoints + subnodes[i]->get_nof_datapoints();
	}

	//get the number of data points on this node
	for (int i = 0; i < nof_monitored; i++) {
		nof_datapoints = nof_datapoints + monitored[i]->nof_sensors;
	}

	return nof_datapoints;
}

void node::set_subnodes(node ** _subnodes, uint8_t _nof_subnodes){
	subnodes = _subnodes;
	nof_subnodes = _nof_subnodes;
}


/*		base node class implementation
 * 	==========================================
 * */

base_node::base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name)
	: node(nullptr, 0, nullptr, 0, nullptr, 0, _name, NONE, NONE){//, nullptr) { //FIXME changed node(nullptr, 0, subnodes, 2, nullptr, 0, _name, NONE, NONE)

	I2C_Handle = _I2C_Handle;
	nof_datapoints = get_nof_datapoints();
	//buffer = new float[nof_datapoints]; //doesn't seem to be working in MCUXPRESSO, replaced with large enough array in 'device_manager.cpp'
}


void base_node::update_buffer(float *databuff) {
	uint16_t buff_cnt = 0; //points to the element in the buffer array to update
	uint16_t failures = read_node(I2C_Handle, buff_cnt, databuff);
	PRINTF("unable to read %d devices \r\n\r\n", failures);
}


//	//allows you to set all IO expanders outputs to the same value
//void base_node::set_expanders_all(uint16_t value[nof_IO_expander]) {
//
//	IO_Expander *expander;
//	uint8_t expander_cnt = 0;
//
//	//store the current configuration of the switches (though is likely to already be off)
//	uint8_t switches_config[] = { switch_0.current_config, switch_1.current_config };
//
//	//todo: broadcasting probably isn't safe, as there is no certainty all devices were reached succesfully.
//	switch_0.set_broadcast(I2C_Handle);
//	switch_1.set_broadcast(I2C_Handle);
//
//
//	for (int j = 0; j < nof_IO_expander; j++) {	//for all non-monitored devices in the RCU nodes
//		if (IO_expanders[j]->type == IO_expander_device) {	//make sure its an IO_expander
//			expander = reinterpret_cast<IO_Expander*>(IO_expanders[j]);	//interpret generic 'device' as IO_expander
////#if LOGGING_GENERAL  == true
//			PRINTF("%s \r\n", IO_expanders[j]->name.c_str());
////#endif
//			expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expanders
//			expander_cnt++;
//		}
//	}
//
//	//restore the previous configuration
//	switch_0.set_configuration(I2C_Handle, switches_config[0]);
//	switch_1.set_configuration(I2C_Handle, switches_config[1]);
//}
//
//void base_node::set_expander_single_node(string RCU_node_name, uint16_t value[nof_IO_expander]) {
//	IO_Expander *expander;
//	uint8_t expander_cnt = 0;
//
//	//remember how the IO switches were configured
//	uint8_t switches_config[2] = { switch_0.current_config, switch_1.current_config };
//	switch_0.set_off(I2C_Handle);	//turn off switch A
//	switch_1.set_off(I2C_Handle);	//turn off switch B
//
//	for (int k = 0; k < nof_switch_nodes; k++) {	//for all subnodes	 (the switch nodes, should be 2)
//
//		for (int i = 0; i < nof_switch_subnodes; i++) {	//for all the subnodes in the subnodes (should be 4)
//
//			if (subnodes[k]->subnodes[i]->name == RCU_node_name) {	//check if this is the correct node
//				subnodes[k]->switcher->set_channel(I2C_Handle, subnodes[k]->subnodes[i]->switch_channel); //set the switch to this channel
//
//				for (int j = 0; j < subnodes[k]->subnodes[i]->nof_devices; j++) {	//for all non-monitored devices in the RCU node
//					if (subnodes[k]->subnodes[i]->devices[j]->type == IO_expander_device) {	//make sure its an IO_expander
//
//						expander = reinterpret_cast<IO_Expander*>(subnodes[k]->subnodes[i]->devices[j]);	//interpret generic 'device' as IO_expander
//						expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expander
//						expander_cnt++;
//					}
//				}
//				subnodes[k]->switcher->set_off(I2C_Handle); //set to off after usage
//			}
//		}
//	}
//	switch_0.set_configuration(I2C_Handle, switches_config[0]);	//restore switch channel settings
//	switch_1.set_configuration(I2C_Handle, switches_config[1]);	//restore switch channel settings
//}

