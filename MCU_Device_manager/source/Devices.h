/*
 * Devices.h
 *
 *  Created on: 27 Mar 2020
 *      Author: snijder
 *
 *      This file provides generic base classes for monitored devices (monitored_device)  and non monitored devices (device)
 *      It also provides a specific implementation per device.
 *
 *      For example: 	pmbus_device is derived from monitored_device
 *      				IO_extender is derived from device
 *
 */

#ifndef DEVICES_H_
#define DEVICES_H_


#include <string>
#include <cmath>

//allow for debugging
#include <fsl_device_registers.h>
#include "fsl_debug_console.h"

//import I2C abstraction functions
extern "C" {
	#include <common.h>
}


#define LOGGING_GENERAL false		//log general messages (E.g sensor names + readout values) (FIXME: tends to fail with FreeRTOS)
#define LOGGING_I2C_FAILURE true	//log whenever I2C reading fails (after retries determined by common.h)
#define IGNORE_SWITCH_ERROR true	//does not skip devices that are unreachable due to I2C switch being unreachable


#define DELAY int cnt = 0;while(cnt < 1000){cnt ++;} cnt = 0; 	//this is for quickly inserting a delay during testing
//#include "tester.h"	//was used while still developing in mcvs, provided I2C "imitation" by simply returning an incrementing value or a constant array of predetermined values

using namespace std;

//pmbus formats enumeration
enum pmbus_format {
	LITERAL,
	VOUT_MODE,
	DIRECT
};

//non-monitored device types
enum device_type {
	I2C_switch_device,
	IO_expander_device

};

#define NONE -1 //useful when referencing a list that can be empty. exists because NULL is simply defined as 0

//monitored device types
class pmbus_sensor { //for instantiating, reading and formatting pmbus sensors
public:
	string name;	//the name of the sensor

	pmbus_sensor(uint8_t _cmd, uint8_t _format = LITERAL, string _name = "pmbus_sensor");
	uint16_t get_measurement(lpi2c_rtos_handle_t *I2C_Handle, uint16_t address, float &datapoint);

private:
	// see the PMBUS specifications for more information about commands and data formats
	float format_vout(uint16_t raw_word, uint8_t raw_byte);
	float format_literal(uint16_t raw_word);
	float format_direct(uint16_t raw_word);	//note: not in use


	uint8_t cmd;	//the command address
	uint8_t format;	//format in which the data is packed
};


class monitored_device { //base class for all I2C devices

public:
	static float counter; //temporary readout replacement, static, increments for every "device readout"
	uint8_t nof_sensors; //used to determine where to store the readout data and how much to reserve

	monitored_device(uint8_t device_address, string device_name = "monitored device", uint8_t _nof_sensors = 1);

	string get_name();
	uint8_t get_address();

	virtual uint16_t device_readout(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &buff_cnt, float *buffer) {
			PRINTF("This is a virtual function that can be used by derived classes to implement how they read out the device");
			return 0;
	}

protected:
	uint8_t address;
	string  name;

};

class pmbus_device : public monitored_device { //derived class from monitored_device for pmbus type devices

public:
	const static uint8_t nof_sensors = 4;	//number of sensors a pmbus device needs to monitor

	pmbus_device(uint8_t device_address, string device_name = "pmbus_device",  pmbus_sensor **_sensors = nullptr);

	uint16_t device_readout(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &buff_cnt, float buffer[]);


private:

	pmbus_sensor **sensors;
};

//pmbus_sensor pmbus_device::sensors[nof_sensors] = {	//create all sensors in a pmbus device, static for all pmbus_device objects
//			pmbus_sensor(0x88, LITERAL, "Input voltage (V)"),
//			pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)"),
//			pmbus_sensor(0x8D, LITERAL, "temperature  (C)"),
//			pmbus_sensor(0x8C, LITERAL, "output current (A)")
//	};

// non-monitored callable device base type
class device {
public:
	device(uint8_t _address, device_type _type, string _name);

	string name;
	device_type type;	//determines to which device type the device needs to be cast
	uint8_t address;

};

class I2C_switch : public device { //class for controlling the {x}CA954{x}A devices
// device has 8 possible addresses, 0x70 - 0x77
// device doesn't use registers, no addressing required, only data
//the 8 channels are encoded in a byte (E.g. bit 0 controls channel 0)

public:
	I2C_switch(uint8_t device_address,  string device_name = "I2C_switch");

	uint8_t current_config = 0;		//hold how the switch is currently configured
	uint8_t off = 0x00;		//the I2C functions require a pointer to a value
	uint8_t broadcast = 0xFF;	//the I2C functions require a pointer to a value

	status_t set_channel(lpi2c_rtos_handle_t *I2C_Handle, uint8_t channel);				//set to 1 specific channel
	status_t set_off(lpi2c_rtos_handle_t *I2C_Handle);									//set all channels to off
	status_t set_broadcast(lpi2c_rtos_handle_t *I2C_Handle);	//set all channels open NOTE: unsafe to use due to lack of (N)ACK for all devices
	status_t set_configuration(lpi2c_rtos_handle_t *I2C_Handle, uint8_t configuration);	//set channels to a specific configuration (E.g. 0b00110010)
};

class IO_Expander : public device{ //IO-expander used for testing. based on the TCA9535 and TCA6416

public:
	IO_Expander(uint8_t device_address,  string device_name = "IO_expander");


	void IO_config(lpi2c_rtos_handle_t *I2C_Handle, uint16_t IO_configuration);	//configure which pins are inputs and outputs (input = 1, output = 0,  default = 1)
	void set_output_values(lpi2c_rtos_handle_t *I2C_Handle, uint16_t out_val);	//set the values of the output pins (no effect on input pins)
	uint16_t read_input_values(); //NOTE: at this time no plans to use any input pins;


private:
	uint16_t input_readout;				//contains the readout values of the input pins
	uint16_t output_values;				//contains the written to the output pins (has no effect for input pins)
	uint16_t configuration_register;	//keeps track of which pins are configured as input(1) or output(0)

	//define the registers in the IO-expander (NOTE:might move this to #define's later)
	static const uint8_t input_port_0_register = 0x00;
	static const uint8_t input_port_1_register = 0x01;
	static const uint8_t output_port_0_register = 0x02;
	static const uint8_t output_port1_register = 0x03;
	static const uint8_t configuration_port_0_register = 0x06;
	static const uint8_t configuration_port_1_register = 0x07;
};


#endif /* DEVICES_H_ */
