/*
 * node.h
 *
 *  Created on: 27 Mar 2020
 *      Author: snijd
 */

#ifndef NODE_H_
#define NODE_H_

//#include <iostream>

#include <string>
#include <cmath>

//#include "fsl_common.h"
//#include "peripherals.h"

#include <fsl_device_registers.h>
#include "fsl_debug_console.h"

//#include "fsl_lpi2c.h"
//#include "fsl_lpi2c_freertos.h"

extern "C" {
	#include <common.h>
}


#include "devices.h"



class node {
public:

	node(monitored_device 	**monitored_ptr_arr = nullptr, 	uint8_t _nof_monitored=0,	// **monitored_ptr_arr: pointer to pointer array of all monitored devices,
		 node 				**subnodes_ptr_arr=nullptr, 	uint8_t _nof_subnodes=0,	// **subnodes_ptr_arr: pointer to pointer array of all subnodes
		 device 			**devices_ptr_arr=nullptr, 		uint8_t _nof_devices=0,		// **devices_ptr_arr: pointer to pointer array of all non-monitored devices
		 string 			_name = "node",
		 int8_t 			_switch_channel = NONE, int8_t _switch_nr = NONE);			//, I2C_switch *_switcher = nullptr)	// data about how to reach this node and where it is located


	uint16_t read_node(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &cnt, float *buffer);
	uint16_t get_nof_datapoints();
	void set_subnodes(node ** _subnodes, uint8_t _nof_subnodes);


	string name;

	int8_t switch_nr;		//tells to which switch of the previous node this node is connected to (NONE means previous node doesn't have a switch)
	int8_t switch_channel;	//to which channel of the previous switch this node is connected to

	I2C_switch *switcher;	//points to the switch object of a node
	// limited to 1 switch currently, see devices/subnodes on how to change if neccecary

	uint8_t nof_subnodes;			// for keeping track how many subnodes this node has
	node ** subnodes;				//pointer to pointer array to all subnodes

	uint8_t nof_monitored;			// for keeping track how many devices this node has
	monitored_device ** monitored;	//pointer to pointer array to all monitored devices

	uint8_t nof_devices;			// for keeping track how many devices this node has
	device ** devices;				//pointer to pointer array to all devices


};



//allows the implementation of functions a base node is interested in and allowing for easier overview of all subnodes
class base_node : public node {

public:
	base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "base_node");

	lpi2c_rtos_handle_t *I2C_Handle;	//reference to the I2C unit used
	uint16_t nof_datapoints = 0;		//counter (during initialisation) of how many monitored datapoints there are

	float readout_buffer[256];	//note: could potentially be a struct carrying additional info

	void update_buffer(float *databuff);	//reads out all the sensors on

//	//allows you to set all IO expanders outputs to the same value
//	void set_expanders_all(uint16_t value[nof_IO_expander]);	//TODO: this function requires access to the expanders and switches, which are not yet defined at this point
//	void set_expander_single_node(string RCU_node_name, uint16_t value[nof_IO_expander]); //TODO: this function requires access to the expanders and switches, which are not yet defined at this point

};



#endif /* NODE_H_ */

