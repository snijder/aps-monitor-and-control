
#include "common.h"

#include "fsl_device_registers.h"
#include "fsl_debug_console.h"

#define RETRIES 10 // the number of consecutive times an I2C read/write action can be retried whenever one fails


//status_t I2C_read_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *raw_byte);
//status_t I2C_read_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *raw_word);
//status_t I2C_read_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t raw_data[], uint8_t data_size);
//
//status_t I2C_write_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *write_byte);
//status_t I2C_write_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *write_word);
//status_t I2C_write_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t write_data[], uint8_t data_size);
//status_t I2C_write_reg(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t write_byte);


status_t I2C_read_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *raw_byte){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = raw_byte;
	transfer_config.dataSize       = 1;
	transfer_config.direction 	   = kLPI2C_Read;

	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
status_t I2C_read_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *raw_word){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = raw_word;
	transfer_config.dataSize       = 2;
	transfer_config.direction 	   = kLPI2C_Read;

	//first atempt
	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
status_t I2C_read_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t raw_data[], uint8_t data_size){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = raw_data;
	transfer_config.dataSize       = data_size;
	transfer_config.direction 	   = kLPI2C_Read;

	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}

status_t I2C_write_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *write_byte){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = write_byte;
	transfer_config.dataSize       = 1;
	transfer_config.direction 	   = kLPI2C_Write;

	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
status_t I2C_write_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *write_word){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = write_word;
	transfer_config.dataSize       = 2;
	transfer_config.direction 	   = kLPI2C_Write;

	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
status_t I2C_write_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t write_data[], uint8_t data_size){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = register_address;
	transfer_config.subaddressSize = sizeof(uint8_t);
	transfer_config.data           = write_data;
	transfer_config.dataSize       = data_size;
	transfer_config.direction 	   = kLPI2C_Write;;

	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
//the I2C switch doesn't have any registers, instead it just configures itself according to the data it received
status_t I2C_write_reg(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t *write_byte){
	status_t status;
	uint8_t retry_cnt = RETRIES;

	lpi2c_master_transfer_t transfer_config = { 0 };	//I2C unit transfer config

	transfer_config.flags          = kLPI2C_TransferDefaultFlag;
	transfer_config.slaveAddress   = device_address;
	transfer_config.subaddress     = 0;
	transfer_config.subaddressSize = 0;
	transfer_config.data           = write_byte;
	transfer_config.dataSize       = 1;
	transfer_config.direction 	   = kLPI2C_Write;

	for(int i = 0; i < 10000; i++);
	status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

	//keep retrying until the max number of retries has been reached
	while(status != kStatus_Success && retry_cnt > 0){
		PRINTF(". ");
		retry_cnt--;
		status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
	}

	return status;
}
/*
 * common.c
 *
 *  Created on: Mar 13, 2020
 *      Author: snijder
 */

////this function should be called whenever the base_node::update_buffer() function fails
//void I2C_error_handler(struct I2C_problem_tracker problems){
//
//}

