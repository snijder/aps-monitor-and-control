/*
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

/* Freescale includes. (most are partially generated through MCUxpresso SDK)*/
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "peripherals.h"
#include "board.h"
#include "fsl_gpio.h"
#include "fsl_lpi2c.h"
#include "pin_mux.h"				//generated based on configuration
#include "clock_config.h"			//generated based on configuration


#include "base_nodes_config.h"
extern "C" {
	#include <common.h>	//includes self witten I2C function on top of the RTOS transfer function
}

#include <string>

//using namespace std;

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TASK_PRIO 			(configMAX_PRIORITIES - 2)		//the priority of the main tasks, all have the second highest priority
#define NOF_I2C_UNITS 		4		//there are a total of 4 I2C units and 2 additional flexio's available to use for I2C, only the I2C units are used
//#define QUEUE_LENGTH 		8		//the queue for receiving commands from OPC ua
#define TICK_PERIOD_MS 		portTICK_PERIOD_MS	//how many ms there are in a tick (note: unsigned int, beware low number)


/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void Simple_read(void *pvParameters);			//this is a simple task that simply reads 1 sensor of 1 device on 1 I2C unit

static void I2C_1_manager_task(void *pvParameters);		//manages all devices on I2C1 unit
static void I2C_2_manager_task(void *pvParameters);		//manages all devices on I2C2 unit
static void I2C_3_manager_task(void *pvParameters);		//manages all devices on I2C3 unit
static void I2C_4_manager_task(void *pvParameters);		//manages all devices on I2C4 unit


/*******************************************************************************
 * Globals
 ******************************************************************************/
//create a pointer array to the various I2C rtos handles
lpi2c_rtos_handle_t *I2C_handle[NOF_I2C_UNITS] = {&LPI2C1_masterHandle, &LPI2C2_masterHandle, &LPI2C3_masterHandle, &LPI2C4_masterHandle};



/*******************************************************************************
 * Code
 ******************************************************************************/
/*!
 * @brief Main function
 */


int main(void)
{
    /* Init board hardware. */
    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();

	//pull up for I2C2, these pins are required to be low during reset, otherwise the device will boot wrong
	//here, after reset, the pins can be pulled up. The I2C2 pins are connected to this pin through 2.2k resistors on each pin
	GPIO_PinWrite(GPIO1, 27U, 1U);	//tp49
    BOARD_InitBootPeripherals();	//initialise all peripherals (in this case only I2C)


	//Freertos reserves highest priority for its own use, set interrupt priority lower.
    NVIC_SetPriority(LPI2C1_IRQn, 2);
    NVIC_SetPriority(LPI2C2_IRQn, 2);
    NVIC_SetPriority(LPI2C3_IRQn, 2);
    NVIC_SetPriority(LPI2C4_IRQn, 2);

//    if (xTaskCreate(I2C_1_manager_task, "I2C_1_manager_task", 2048, (void*)NULL, TASK_PRIO, NULL) != pdPASS){
//    		PRINTF("I2C_1_manager_task creation failed!.\r\n");
//    		while (1);
//    }

//    if (xTaskCreate(I2C_2_manager_task, "I2C_2_manager_task", 2048, (void*)NULL, TASK_PRIO, NULL) != pdPASS){
//    		PRINTF("I2C_2_manager_task creation failed!.\r\n");
//    		while (1);
//    }

    if (xTaskCreate(I2C_3_manager_task, "I2C_3_manager_task", 2048, (void*)NULL, TASK_PRIO, NULL) != pdPASS){
    		PRINTF("I2C_3_manager_task creation failed!.\r\n");
    		while (1);
    }

//    if (xTaskCreate(I2C_4_manager_task, "I2C_4_manager_task", 2048, (void*)NULL, TASK_PRIO, NULL) != pdPASS){
//    		PRINTF("I2C_4_manager_task creation failed!.\r\n");
//    		while (1);
//    }



	//simply reads 1 sensor of 1 device, known to work, can be used to confirm I2C unit is wired correctly
	if (xTaskCreate(Simple_read, "Simple_read", 1024, (uint8_t *)1U, TASK_PRIO, NULL) != pdPASS){
		PRINTF("The \'simple_read\' task creation failed.\r\n");
		while (1);
	}


    /* Start scheduling. */
    vTaskStartScheduler();
    for (;;);
}

static void I2C_1_manager_task(void *pvParameters){
	PRINTF("initialising I2C_unit_1 base node\r\n");

	TEST_base_node I2C_unit(I2C_handle[0], "I2C_unit_1");	//create the base node object

	float data_buff[256];//array, large enough all datapoints in the base node
	I2C_unit.update_buffer(data_buff);

	for(int i = 0; i < 256; i++){
		PRINTF("%d:\t%f\r\n", i, data_buff[i]);
	}

	for(;;){
		I2C_unit.update_buffer(data_buff);
		//use freeRTOS "vApplicationTickHook" to accurately sync to every second instead of just waiting 1000ms
		vTaskDelay(1000 / TICK_PERIOD_MS);
	}
}

static void I2C_2_manager_task(void *pvParameters){
	PRINTF("initialising I2C_unit_2 base node\r\n");

	TEST_base_node I2C_unit(I2C_handle[1], "I2C_unit_2");	//create the base node object


	float data_buff[256]; //array, large enough to fit any base node.
	I2C_unit.update_buffer(data_buff);

	for(int i = 0; i < 256; i++){
		PRINTF("%d:\t%f\r\n", i, data_buff[i]);
	}

	for(;;){
		I2C_unit.update_buffer(data_buff);

		vTaskDelay(1000 / TICK_PERIOD_MS);
	}
}

static void I2C_3_manager_task(void *pvParameters){
	PRINTF("initialising I2C_unit_3 base node\r\n");

	TEST_base_node I2C_unit(I2C_handle[2], "I2C_unit_3");	//create the base node object


	float data_buff[256]; //array, large enough all datapoints in the base node
	I2C_unit.update_buffer(data_buff);

	for(int i = 0; i < 256; i++){
		PRINTF("%d:\t%f\r\n", i, data_buff[i]);
	}

	for(;;){
		I2C_unit.update_buffer(data_buff);

		vTaskDelay(1000 / TICK_PERIOD_MS);
	}
}

static void I2C_4_manager_task(void *pvParameters){
	PRINTF("initialising I2C_unit_4 base node\r\n");

	TEST_base_node I2C_unit(I2C_handle[3], "I2C_unit_4");	//create the base node object


	float data_buff[256];//array, large enough all datapoints in the base node
	I2C_unit.update_buffer(data_buff);

	for(int i = 0; i < 256; i++){
		PRINTF("%d:\t%f\r\n", i, data_buff[i]);
	}

	for(;;){
		I2C_unit.update_buffer(data_buff);

		vTaskDelay(1000 / TICK_PERIOD_MS);
	}
}

static void Simple_read(void *pvParameters)
{

	status_t status;

    uint16_t raw_word = 0;

    for (;;)
    {
    	status = I2C_read_word(I2C_handle[2], 0x4F, 0x8D, &raw_word);

    	if(status == kStatus_Success){
    		PRINTF("tester: Received:\t%hX\r\n", raw_word);

    		//lines of code extracted from pmbus_sensor::format_literal function
    		float output = 0;

    		uint16_t mantissa = raw_word & 0b0000011111111111;  // mask upper 5 bits to 0
    		uint16_t exponent = raw_word >> 11;  // shift exponent to the rightmost bit

    		if ((exponent & 0b10000) >= 1) {  // if the exponent is negative(MSB == 1)
    			exponent = exponent & 0b01111;
    			exponent = exponent - 16;
    		}
    		if ((mantissa & 0b10000000000) >= 1) {  // if the mantissa is negative(MSB == 1)
    			mantissa = mantissa & 0b01111111111;
    			mantissa = mantissa - 1024;

    		}

    		output = float(int16_t(mantissa) * pow(2, int8_t(exponent)));  // value = mantissa * 2 ^ exponent

    		printf("%s: formatted: %f\r\n\r\n", pcTaskGetName(NULL), output);
    	}
    	else if(status == kStatus_LPI2C_Busy){
    		//the busy flag seems to be triggered when handling the wires carrying the signal while running
    		//the busy error does not seem to go away once triggered. TODO: maybe add reset whenever this happens
    		PRINTF("%s: I2C unit is busy \r\n", pcTaskGetName(NULL));
    	}
    	else {
    		//all other errors here
    		PRINTF("%s: I2C error occured \r\n", pcTaskGetName(NULL));
    	}
    	vTaskDelay(1000 / TICK_PERIOD_MS);
    }
}


//FreeRTOS funtions (enabled in FreeRTOSConfig.h)
extern "C" {
	//gets called whenever memory allocation fail occurs
	void vApplicationMallocFailedHook(){
		PRINTF("Oh boy, time to die! (Malloc fail)\r\n");
		for(;;){
			vTaskDelay(pdMS_TO_TICKS(1000));
		}
	}

	//gets called whenever a stack overflow occurs
	void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName ){
		PRINTF("Oh boy, time to die! (stack overflow)\r\n");
		for(;;){
			vTaskDelay(pdMS_TO_TICKS(1000));
		}
	}

	//if no other tasks are available DO NOT YIELD THIS TASK
	void vApplicationIdleHook(void)
	{
//	    volatile size_t xFreeStackSpace = xPortGetFreeHeapSize();
		for(;;);

	}
}
