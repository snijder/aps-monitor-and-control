/*
 * data_structs.h
 *
 *  Created on: Mar 12, 2020
 *      Author: snijder
 *
 *  Description:
 *  This code provides serveral common functions, data types and structs for the project
 */

#ifndef DATA_STRUCTS_H_
#define DATA_STRUCTS_H_




#include "fsl_common.h"
#include "peripherals.h"


//enum direction {
//    read,
//    write
//};
////struct for I2C transfer input
//struct I2C_transfer{
//	enum direction direction;
//	status_t status;
//	uint8_t device_address;
//	uint8_t register_address;
//	uint8_t data_size;
//	uint8_t *data_ptr;
//};
//
//struct I2C_data{
//	uint8_t device_address;
//	uint8_t register_address;
//	uint8_t data_size;
//	uint8_t *data_ptr;
//};

//struct I2C_problem_tracker {
//	uint16_t failure_cnt;			//counts how many times there was a communication failure each readout update
//	uint8_t switch_failure_cnt;	//counts whenever a switch fails (important because that means all its subnodes are unreachable)
//	uint8_t skip_cnt;				//counts the data points skipped because communication with a switch failed.
//};
//void I2C_error_handler(struct I2C_problem_tracker problems);



status_t I2C_read_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *raw_byte);
status_t I2C_read_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *raw_word);
status_t I2C_read_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t raw_data[], uint8_t data_size);

status_t I2C_write_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t *write_byte);
status_t I2C_write_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint16_t *write_word);
status_t I2C_write_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t register_address, uint8_t write_data[], uint8_t data_size);
status_t I2C_write_reg(lpi2c_rtos_handle_t *I2C_Handle, uint8_t device_address, uint8_t *write_byte);

#endif /* DATA_STRUCTS_H_ */
