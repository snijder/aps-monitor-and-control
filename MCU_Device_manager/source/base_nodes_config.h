/*
 * base_nodes_config.h
 *
 *  Created on: 27 Mar 2020
 *      Author: snijd
 *
 *
 *      This file can be used to provide the configurations for each I2C unit (base node)
 */

#ifndef BASE_NODES_CONFIG_H_
#define BASE_NODES_CONFIG_H_

#include "node.h"


//root node used during testing
class TEST_base_node : public base_node {
public:
	TEST_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "TEST_base_node")
		: base_node(_I2C_Handle, _name){

		set_subnodes(base_subnodes, base_nof_subnodes);
		nof_datapoints = get_nof_datapoints();
	}

private:

/*===================================================================================
		test base node configuration starts here
===================================================================================*/

	//all sensors of a pmbus device we're interested in
	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
	//reference all sensors in a pointer array
	static const uint8_t nof_sensors = 4;
	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};


	// create all devices that are constantly monitored
	pmbus_device BMR451_0 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
	pmbus_device BMR462_1 = pmbus_device(0x65, "BMR462_0x65", sensors);
	pmbus_device BMR451_2 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
	pmbus_device BMR462_3 = pmbus_device(0x65, "BMR462_0x65", sensors);
	pmbus_device BMR451_4 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
	pmbus_device BMR462_5 = pmbus_device(0x65, "BMR462_0x65", sensors);
	pmbus_device BMR451_6 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
	pmbus_device BMR462_7 = pmbus_device(0x65, "BMR462_0x65", sensors);


	static const uint8_t nof_monitored = 8;	//input here the number of devices on an FPGA_node
	monitored_device *monitored[nof_monitored] = { 	&BMR451_0, &BMR462_1, &BMR451_2, &BMR462_3,
													&BMR451_4, &BMR462_5, &BMR451_6, &BMR462_7};

	//non monitored devices
	IO_Expander IO_0 = IO_Expander(0x73, "IO_expander 0");
	IO_Expander IO_1 = IO_Expander(0x74, "IO_expander 1");
	IO_Expander IO_2 = IO_Expander(0x75, "IO_expander 2");

	//create pointer array of all non-monitored devices in a node
	static const uint8_t nof_IO_expander = 3;
	device  *IO_expanders[nof_IO_expander] = { &IO_0, &IO_1, &IO_2};

	//create all the subnodes attached to a switch (*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)

	//switch A
	node subnode_A = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_A", 0);
	node subnode_B = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_B", 1);
	node subnode_C = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_C", 2);
	node subnode_D = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_D", 3);

	//switch B
	node subnode_E = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_E", 0);
	node subnode_F = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_F", 1);
	node subnode_G = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_G", 2);
	node subnode_H = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_H", 3);

	//reference all subnodes in a pointer array
	static const uint8_t nof_switch_subnodes = 4; //each switch has 4 subnodes attached to it
	node * switch_subnodes_SW_0[nof_switch_subnodes] = { &subnode_A, &subnode_B, &subnode_C, &subnode_D};
	node * switch_subnodes_SW_1[nof_switch_subnodes] = { &subnode_E, &subnode_F, &subnode_G, &subnode_H};


	//these switches are used to select the correct subnode channel
	I2C_switch switch_0 = I2C_switch(0x70, "I2C_switch_0"); //can have 0x70 to 0x77
	I2C_switch switch_1 = I2C_switch(0x70, "I2C_switch_1");	//can have 0x70 to 0x77
	device * switch_0_ptr[1] = { &switch_0 };
	device * switch_1_ptr[1] = { &switch_0 };

	// create the RCU switch nodes
	node subnode_0 = node(nullptr, 0, switch_subnodes_SW_0, nof_switch_subnodes, switch_0_ptr, 1, "switch node A", NONE, 0);
	node subnode_1 = node(nullptr, 0, switch_subnodes_SW_1, nof_switch_subnodes, switch_1_ptr, 1, "switch node B", NONE, 1);

	//create a pointer array pointing to the switch subnodes
	static const uint8_t base_nof_subnodes = 2;
	node *base_subnodes[base_nof_subnodes]{ &subnode_0,  &subnode_1 };

};
/*===================================================================================
		TEST base node configuration ends here
===================================================================================*/



/*===================================================================================
		RCU base node configuration starts here
===================================================================================*/
class RCU_base_node : public base_node { //the base nodes that contain the RCU's (1 for RCU 0 - 15 and 1 for 16 - 31)
public:
	RCU_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "RCU2_base_node", uint8_t starting_rcu_num=0)
		: base_node(_I2C_Handle, _name){

		set_subnodes(base_subnodes, nof_switch_nodes);

		//ensures the RCU's are named 0 to 31 correctly
		for (int i = starting_rcu_num; i < starting_rcu_num + 16; i++) {
			if (i < 8 + starting_rcu_num) {
				RCU_switch_subnodes_SW_0[i - starting_rcu_num]->name = "RCU_" + to_string(i);
			}
			else {
				RCU_switch_subnodes_SW_1[i - 8 - starting_rcu_num]->name = "RCU_" + to_string(i);
			}
		}
	}

private:


	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
	//reference all sensors in a pointer array
	static const uint8_t nof_sensors = 4;
	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};


	// create all devices on an RCU that are constantly monitored
	pmbus_device generic_device_A = pmbus_device(0x65, "test_module 0x65", sensors);
	pmbus_device generic_device_B = pmbus_device(0x4F, "test_module 0x4F", sensors);

	//reference all devices on an FPGA_NODE into a pointer array
	static const uint8_t nof_RCU_monitored = 2;	//input here the number of devices on an FPGA_node
	monitored_device *RCU_monitored[nof_RCU_monitored] = { &generic_device_A, &generic_device_B };


	IO_Expander IO_0 = IO_Expander(0x20, "TCA9539, IO_expander 0");
	IO_Expander IO_1 = IO_Expander(0x21, "TCA9539, IO_expander 1");
	IO_Expander IO_2 = IO_Expander(0x22, "TCA6416, IO_expander 2");

	static const uint8_t nof_IO_expander_per_RCU = 3;
	device  *IO_expanders[nof_IO_expander_per_RCU] = { &IO_0, &IO_1, &IO_2};

	//create all the RCU's attached to a switch (*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)
	node RCU_0 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 0);
	node RCU_1 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 1);
	node RCU_2 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 2);
	node RCU_3 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 3);
	node RCU_4 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 4);
	node RCU_5 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 5);
	node RCU_6 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 6);
	node RCU_7 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 7);

	node RCU_8 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 0);
	node RCU_9 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 1);
	node RCU_10 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 2);
	node RCU_11 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 3);
	node RCU_12 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 4);
	node RCU_13 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 5);
	node RCU_14 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 6);
	node RCU_15 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 7);

	//reference all subnodes in a pointer array
	static const uint8_t nof_RCU_switch_subnodes = 8; //each RCU switch has 8 RCU's attached to it
	node * RCU_switch_subnodes_SW_0[nof_RCU_switch_subnodes] = { &RCU_0, &RCU_1, &RCU_2, &RCU_3,
															&RCU_4, &RCU_5, &RCU_6, &RCU_7 };

	node * RCU_switch_subnodes_SW_1[nof_RCU_switch_subnodes] = { &RCU_8, &RCU_9, &RCU_10, &RCU_11,
															&RCU_12, &RCU_13, &RCU_14, &RCU_15 };


	//these switches are used by the uniboards to select the correct subnode channel
	I2C_switch RCU_switch_0 = I2C_switch(0x70, "I2C_switch_0"); //can have 0x70 to 0x77
	I2C_switch RCU_switch_1 = I2C_switch(0x71, "I2C_switch_1");	//can have 0x70 to 0x77
	device * switch_0[1] = { &RCU_switch_0 };
	device * switch_1[1] = { &RCU_switch_1 };

	// create the RCU switch nodes
	node RCU_SW_node_0 = node(nullptr, 0, RCU_switch_subnodes_SW_0, nof_RCU_switch_subnodes, switch_0, 1, "RCU switch A", NONE, 0);// , &RCU_switch_0);
	node RCU_SW_node_1 = node(nullptr, 0, RCU_switch_subnodes_SW_1, nof_RCU_switch_subnodes, switch_1, 1, "RCU switch B", NONE, 1);// , &RCU_switch_1);

	//create a pointer array pointing to the RCU switches
	static const uint8_t nof_switch_nodes = 2;
	node *base_subnodes[nof_switch_nodes]{ &RCU_SW_node_0,  &RCU_SW_node_1 };

};

/*===================================================================================
		RCU base node configuration ends here
===================================================================================*/



/*===================================================================================
		UNIBOARD base node configuration starts here
===================================================================================*/

class unb_base_node : public base_node { //the base nodes that contain the uniboards (node with unb 0, 1 and node with unb 2, 3)
public:
	unb_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "uniboard", uint8_t starting_board_nr=0)
		: base_node(_I2C_Handle, _name) {//, nullptr) {

		set_subnodes(base_subnodes, nof_uniboards);

		uniboard_A.name = "uniboard_" + to_string(starting_board_nr);
		uniboard_A.name = "uniboard_" + to_string(starting_board_nr + 1);
		I2C_Handle = _I2C_Handle;

		nof_datapoints = get_nof_datapoints();
	}

private:

	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
	//reference all sensors in a pointer array
	static const uint8_t nof_sensors = 4;
	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};


	//create all the devices on the SENS node of the uniboard
	pmbus_device SENS_pol0 = pmbus_device(0x01, "BMR464, QSFP0 power supply", sensors);
	pmbus_device SENS_pol1 = pmbus_device(0x02, "BMR464, QSFP1 power supply", sensors);
	pmbus_device SENS_pol2 = pmbus_device(0x0D, "BMR461, CLK power supply", sensors);
	pmbus_device SENS_pol3 = pmbus_device(0x0E, "BMR461, 3V3 power supply", sensors);
	pmbus_device SENS_pol4 = pmbus_device(0x0F, "BMR461, 1V2 power supply", sensors);
	pmbus_device SENS_PIM = pmbus_device(0x2B, "PIM4328PD, power input module", sensors);
	pmbus_device SENS_DCDC = pmbus_device(0x2C, "BMR456 , DC/DC converter", sensors);
	monitored_device SENS_temperature0 = monitored_device(0x4C, "TMP451, Temp. sensor node 0");
	monitored_device SENS_temperature1 = monitored_device(0x4D, "TMP451, Temp. sensor node 1");
	monitored_device SENS_temperature2 = monitored_device(0x4E, "TMP451, Temp. sensor node 2");
	monitored_device SENS_temperature3 = monitored_device(0x4B, "TMP451, Temp. sensor node 3");
	//reference all devices on the SENS node into a pointer array
	static const uint8_t nof_sens_devices = 11; //input here the number of devices on the SENS node
	monitored_device * SENS_devices[nof_sens_devices] = {
						&SENS_pol0, &SENS_pol1, &SENS_pol2, &SENS_pol3, &SENS_pol4, &SENS_PIM, &SENS_DCDC,
						&SENS_temperature0, &SENS_temperature1, &SENS_temperature2, &SENS_temperature3 };


	// create all devices on an FPGA node on the uniboard
	pmbus_device FPGA_NODE_pol0 = pmbus_device(0x01, "BMR464, Core supply", sensors);
	pmbus_device FPGA_NODE_pol1 = pmbus_device(0x0D, "BMR461, VCC RAM", sensors);
	pmbus_device FPGA_NODE_pol2 = pmbus_device(0x0E, "BMR461, Tranceiver power supply 0", sensors);
	pmbus_device FPGA_NODE_pol3 = pmbus_device(0x0F, "BMR461, Tranceiver power supply 1", sensors);
	pmbus_device FPGA_NODE_pol4 = pmbus_device(0x10, "BMR461, control power supply", sensors);
	pmbus_device FPGA_NODE_pol5 = pmbus_device(0x11, "BMR461, FPGA IO power supply ", sensors);

	//reference all devices on an FPGA_NODE into a pointer array
	static const uint8_t nof_fpga_node_devices = 6;	//input here the number of devices on an FPGA_node
	monitored_device *FPGA_NODE_devices[nof_fpga_node_devices] = { &FPGA_NODE_pol0, &FPGA_NODE_pol1, &FPGA_NODE_pol2,
																   &FPGA_NODE_pol3, &FPGA_NODE_pol4, &FPGA_NODE_pol5 };


	// create all nodes on an FPGA node(*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)
	node unb_fpga_node_0 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_0", 0);
	node unb_fpga_node_1 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_1", 1);
	node unb_fpga_node_2 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_2", 2);
	node unb_fpga_node_3 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_3", 3);
	node unb_sens_node = node(SENS_devices, nof_sens_devices, nullptr, 0, nullptr, 0, "unb_sens_node", 4);

	//reference all subnodes in a pointer array
	static const uint8_t nof_unb_subnodes = 5; //the number of subnodes a uniboard has
	node * uniboard_subnodes[nof_unb_subnodes] = { &unb_fpga_node_0, &unb_fpga_node_1,
												   &unb_fpga_node_2, &unb_fpga_node_3, &unb_sens_node };

	//these switches are used by the uniboards to select the correct subnode channel
	I2C_switch uniboard_A_switch = I2C_switch(0x70); //can have 0x70 to 0x77
	I2C_switch uniboard_B_switch = I2C_switch(0x71); //can have 0x70 to 0x77
	device * switch_A[1] = { &uniboard_A_switch };
	device * switch_B[1] = { &uniboard_B_switch };

	//create both uniboards (note: for a 2 unb APS it is possible to remove the uniboard here, or use one I2C unit less
	node uniboard_A = node(nullptr, 0, uniboard_subnodes, nof_unb_subnodes, switch_A, 1, "uniboard A", NONE, 0);// , &uniboard_A_switch&);		//create uniboard 0
	node uniboard_B = node(nullptr, 0, uniboard_subnodes, nof_unb_subnodes, switch_B, 1, "Uniboard B", NONE, 1);// , &uniboard_B_switch);		//create uniboard 1

	//create a pointer array pointing to the uniboards
	static const uint8_t nof_uniboards = 2;
	node *base_subnodes[nof_uniboards]{ &uniboard_A,  &uniboard_B };
};
/*===================================================================================
		UNIBOARD base node configuration ends here
===================================================================================*/


#endif /* BASE_NODES_CONFIG_H_ */
