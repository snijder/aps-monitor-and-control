################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.c 

OBJS += \
./amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.o 

C_DEPS += \
./amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F/port.d 


# Each subdirectory must supply rules for building sources it contributes
amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F/%.o: ../amazon-freertos/freertos_kernel/portable/GCC/ARM_CM4F/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu11 -D__NEWLIB__ -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\projects\MCUXpresso\Device_manager\board" -I"D:\projects\MCUXpresso\Device_manager\source" -I"D:\projects\MCUXpresso\Device_manager" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\Device_manager\drivers" -I"D:\projects\MCUXpresso\Device_manager\device" -I"D:\projects\MCUXpresso\Device_manager\CMSIS" -I"D:\projects\MCUXpresso\Device_manager\xip" -I"D:\projects\MCUXpresso\Device_manager\drivers\freertos" -I"D:\projects\MCUXpresso\Device_manager\utilities" -I"D:\projects\MCUXpresso\Device_manager\component\serial_manager" -I"D:\projects\MCUXpresso\Device_manager\component\lists" -I"D:\projects\MCUXpresso\Device_manager\component\uart" -I"D:\projects\MCUXpresso\Device_manager\component\timer" -I"D:\projects\MCUXpresso\Device_manager\component\gpio" -I"D:\projects\MCUXpresso\Device_manager\component\i2c" -I"D:\projects\MCUXpresso\Device_manager\component\button" -I"D:\projects\MCUXpresso\Device_manager\component\timer_manager" -I"D:\projects\MCUXpresso\Device_manager\component\led" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


