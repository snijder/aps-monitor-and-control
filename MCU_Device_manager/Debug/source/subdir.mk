################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../source/Device_manager.cpp \
../source/Devices.cpp \
../source/cpp_config.cpp \
../source/node.cpp 

C_SRCS += \
../source/common.c \
../source/semihost_hardfault.c 

OBJS += \
./source/Device_manager.o \
./source/Devices.o \
./source/common.o \
./source/cpp_config.o \
./source/node.o \
./source/semihost_hardfault.o 

CPP_DEPS += \
./source/Device_manager.d \
./source/Devices.d \
./source/cpp_config.d \
./source/node.d 

C_DEPS += \
./source/common.d \
./source/semihost_hardfault.d 


# Each subdirectory must supply rules for building sources it contributes
source/%.o: ../source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -std=gnu++14 -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__NEWLIB__ -I"D:\projects\MCUXpresso\Device_manager\board" -I"D:\projects\MCUXpresso\Device_manager\source" -I"D:\projects\MCUXpresso\Device_manager" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\Device_manager\drivers" -I"D:\projects\MCUXpresso\Device_manager\device" -I"D:\projects\MCUXpresso\Device_manager\CMSIS" -I"D:\projects\MCUXpresso\Device_manager\xip" -I"D:\projects\MCUXpresso\Device_manager\drivers\freertos" -I"D:\projects\MCUXpresso\Device_manager\utilities" -I"D:\projects\MCUXpresso\Device_manager\component\serial_manager" -I"D:\projects\MCUXpresso\Device_manager\component\lists" -I"D:\projects\MCUXpresso\Device_manager\component\uart" -I"D:\projects\MCUXpresso\Device_manager\component\timer" -I"D:\projects\MCUXpresso\Device_manager\component\gpio" -I"D:\projects\MCUXpresso\Device_manager\component\i2c" -I"D:\projects\MCUXpresso\Device_manager\component\button" -I"D:\projects\MCUXpresso\Device_manager\component\timer_manager" -I"D:\projects\MCUXpresso\Device_manager\component\led" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fno-rtti -fno-exceptions -v -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

source/%.o: ../source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu11 -D__NEWLIB__ -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DFSL_RTOS_BM -DSDK_OS_BAREMETAL -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -I"D:\projects\MCUXpresso\Device_manager\board" -I"D:\projects\MCUXpresso\Device_manager\source" -I"D:\projects\MCUXpresso\Device_manager" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\Device_manager\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\Device_manager\drivers" -I"D:\projects\MCUXpresso\Device_manager\device" -I"D:\projects\MCUXpresso\Device_manager\CMSIS" -I"D:\projects\MCUXpresso\Device_manager\xip" -I"D:\projects\MCUXpresso\Device_manager\drivers\freertos" -I"D:\projects\MCUXpresso\Device_manager\utilities" -I"D:\projects\MCUXpresso\Device_manager\component\serial_manager" -I"D:\projects\MCUXpresso\Device_manager\component\lists" -I"D:\projects\MCUXpresso\Device_manager\component\uart" -I"D:\projects\MCUXpresso\Device_manager\component\timer" -I"D:\projects\MCUXpresso\Device_manager\component\gpio" -I"D:\projects\MCUXpresso\Device_manager\component\i2c" -I"D:\projects\MCUXpresso\Device_manager\component\button" -I"D:\projects\MCUXpresso\Device_manager\component\timer_manager" -I"D:\projects\MCUXpresso\Device_manager\component\led" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-sp-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


