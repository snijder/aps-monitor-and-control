///*
// * classes.h
// *
// *  Created on: Mar 12, 2020
// *      Author: snijder
// *
// *  Description:
// *
// */
//
//#ifndef CLASSES_H_
//#define CLASSES_H_
//
//
//
//
////#include <iostream>
//
//#include <string>
//#include <cmath>
//
////#include "fsl_common.h"
////#include "peripherals.h"
//
//#include <fsl_device_registers.h>
//#include "fsl_debug_console.h"
//
////#include "fsl_lpi2c.h"
////#include "fsl_lpi2c_freertos.h"
//
//extern "C" {
//	#include <common.h>
//}
//
//
//#include "devices.h"

//#define LOGGING_GENERAL false		//log general messages (E.g sensor names + readout values) (FIXME: tends to fail with FreeRTOS)
//#define LOGGING_I2C_FAILURE true	//log whenever I2C reading fails (after retries determined by common.h)
//#define IGNORE_SWITCH_ERROR true	//does not skip devices that are unreachable due to I2C switch being unreachable
//
//
//#define DELAY int cnt = 0;while(cnt < 1000){cnt ++;} cnt = 0; 	//this is for quickly inserting a delay during testing
////#include "tester.h"	//was used while still developing in mcvs, provided I2C "imitation" by simply returning an incrementing value or a constant array of predetermined values
//
//using namespace std;
//
////pmbus formats enumeration
//enum pmbus_format {
//	LITERAL,
//	VOUT_MODE,
//	DIRECT
//};
//
////non-monitored device types
//enum device_type {
//	I2C_switch_device,
//	IO_expander_device
//
//};
//
//#define NONE -1 //useful when referencing a list that can be empty. exists because NULL is simply defined as 0

//
//class node {
//public:
//
//	node(monitored_device **monitored_ptr_arr = nullptr, uint8_t _nof_monitored=0,	// **monitored_ptr_arr: pointer to pointer array of all monitored devices,
//		 node **subnodes_ptr_arr=nullptr, uint8_t _nof_subnodes=0,					// **subnodes_ptr_arr: pointer to pointer array of all subnodes
//		 device **devices_ptr_arr=nullptr, uint8_t _nof_devices=0,					// **devices_ptr_arr: pointer to pointer array of all non-monitored devices
//		 string _name = "node",
//		 int8_t _switch_channel = NONE, int8_t _switch_nr = NONE)//, I2C_switch *_switcher = nullptr)	// data about how to reach this node and where it is located
//	{
//		name = _name;
//
//		switch_nr = _switch_nr;				//to which switch this node is connected to (NONE means previous node doesn't have a switch)
//		switch_channel	= _switch_channel;	//to which channel of a switch this node is connected to
//		//switcher = _switcher;				//pointer to the switch that controls this node
//
//		nof_monitored = _nof_monitored;
//		monitored = monitored_ptr_arr;
//
//		nof_subnodes = _nof_subnodes;
//		subnodes = subnodes_ptr_arr;
//
//		nof_devices = _nof_devices;
//		devices = devices_ptr_arr;
//
//		//check if any of the devices are an I2C switch, if so, set the switch pointer to it(only one switch allowed)
//		for (int i = 0; i < _nof_devices; i++) {
//			if (devices_ptr_arr[i]->type == I2C_switch_device) {
//				switcher = reinterpret_cast<I2C_switch*>(devices_ptr_arr[i]);
//				break;
//			}
//		}
//	}
//	// Copy constructor
////	node(const node &other_node) {
////		nof_subnodes = other_node.nof_subnodes,		subnodes = other_node.subnodes,
////			nof_devices = other_node.nof_devices,	devices = other_node.devices,
////			monitored = other_node.monitored,		nof_monitored = other_node.nof_monitored,
////			switch_nr = other_node.switch_nr,		switch_channel = other_node.switch_channel,
////			switcher = other_node.switcher,			name = other_node.name;
////	}
//
////	string get_name() { //this took ages to get right /s
////		return name;	//who knew names were so complex /s
////	}
//
//
//	uint16_t read_node(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &cnt, float *buffer) { //read all the devices on this node and its subnodes
//		status_t status = -1;		//holds the status of the I2C read
//		uint16_t failure_cnt = 0;	//whenever setting a switch fails, this counter is incremented
//
//
//#if LOGGING_GENERAL == true
//		PRINTF( "entering node: %s\r\n", name.c_str());
//#endif
//
//		for (int i = 0; i < nof_monitored; i++) { //go through all devices on this node
//			failure_cnt+= monitored[i]->device_readout(I2C_Handle, cnt, buffer);
//		}
//
//		for (int i = 0; i < nof_subnodes; i++) {	//go through the subnodes and read all their devices and then subnodes
//			if (switch_nr != NONE && switcher != nullptr) {				// if this node contains a switch
//
//#if LOGGING_GENERAL == true
//				PRINTF("%s - switching to channel: %d", subnodes[i]->name.c_str(), int(subnodes[i]->switch_channel));
//				PRINTF( ", of switch: %d, %s located on: %s\r\n", int(switch_nr), switcher->name.c_str(), name.c_str());
//#endif
//
//				status = switcher->set_channel(I2C_Handle, subnodes[i]->switch_channel);	//change the selected channel to that of the next sub node
//
//
//#if IGNORE_SWITCH_ERROR == false	//if true, skips nodes that become unreachable whenever setting the switch fails
//				if(status == kStatus_Success ){
//					failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
//				}
//				else{		//if the switching failed, abort reading the subnodes, as this will likely result in faulty data
//					//TODO: for now only skips all devices (keeping old values), it would be useful to track over time
//
//					cnt+= subnodes[i]->get_nof_datapoints();	//skip this many datapoints
//
//					failure_cnt++;
//				}
//#else
//				if(status != kStatus_Success ){
//					PRINTF("%s: \t", name.c_str());
//					failure_cnt ++;
//				}
//				failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
//#endif
//			}
//			else {	//if there is no switch to worry about
//				failure_cnt += subnodes[i]->read_node(I2C_Handle, cnt, buffer);
//			}
//
//
//
//		}
//		if (switch_nr != NONE && switcher != nullptr) {
//			switcher->set_off(I2C_Handle); //turn of this switch when done
//		}
//		//PRINTF("failures: %d\r\n", failure_cnt);
//		return failure_cnt;
//	}
//
//	uint16_t get_nof_datapoints() {	//counts the total number of datapoints this nodes (its devices and subnodes) has
//		uint16_t nof_datapoints = 0;
//
//		//get the number of data points the subnodes contain
//		for (int i = 0; i < nof_subnodes; i++) {
//			nof_datapoints = nof_datapoints + subnodes[i]->get_nof_datapoints();
//		}
//
//		//get the number of data points on this node
//		for (int i = 0; i < nof_monitored; i++) {
//			nof_datapoints = nof_datapoints + monitored[i]->nof_sensors;
//		}
//
//		return nof_datapoints;
//	}
//
//	string name;
//
//	int8_t switch_nr;		//tells to which switch of the previous node this node is connected to (NONE means previous node doesn't have a switch)
//	int8_t switch_channel;	//to which channel of the previous switch this node is connected to
//
//	I2C_switch *switcher;	//points to the switch object of a node
//	// limited to 1 switch currently, see devices/subnodes on how to change if neccecary
//
//	uint8_t nof_subnodes;			// for keeping track how many subnodes this node has
//	node ** subnodes;				//pointer to pointer array to all subnodes
//
//	uint8_t nof_monitored;			// for keeping track how many devices this node has
//	monitored_device ** monitored;	//pointer to pointer array to all monitored devices
//
//	uint8_t nof_devices;			// for keeping track how many devices this node has
//	device ** devices;				//pointer to pointer array to all devices
//
//};
//
//class unb_base_node : public node { //the base nodes that contain the uniboards (node with unb 0, 1 and node with unb 2, 3)
//public:
//	unb_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "uniboard", uint8_t starting_board_nr=0)
//		: node(nullptr, 0, subnodes, 2,	nullptr, 0, _name, NONE,  NONE) {//, nullptr) {
//		//sprintf(uniboard_A.name, "uniboard_%d",starting_board_nr);		//c style
//		//sprintf(uniboard_B.name, "uniboard_%d",starting_board_nr + 1);	//c style
//
//		uniboard_A.name = "uniboard_" + to_string(starting_board_nr);
//		uniboard_A.name = "uniboard_" + to_string(starting_board_nr + 1);
//		I2C_Handle = _I2C_Handle;
//
//		nof_datapoints = get_nof_datapoints();
//		//buffer = new float[nof_datapoints]; //create a new buffer that contains all the datapoints
//	}
//
//private:
//	lpi2c_rtos_handle_t *I2C_Handle;
///*===================================================================================
//		base node configuration starts here
//===================================================================================*/
//
//	// create the PMBUS sensors that are used
//	// todo: 	static intialisation would work better since this is the same for everything,
//	// 			hoeverer, when i moved this from my test application, that stopped working,
//	//			probably due to this being moved to a header, though moving it to a .cpp didn't work eitehr
//	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
//	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
//	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
//	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
//	//reference all sensors in a pointer array
//	static const uint8_t nof_sensors = 4;
//	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};
//
//
//	//create all the devices on the SENS node of the uniboard
//	pmbus_device SENS_pol0 = pmbus_device(0x01, "BMR464, QSFP0 power supply", sensors);
//	pmbus_device SENS_pol1 = pmbus_device(0x02, "BMR464, QSFP1 power supply", sensors);
//	pmbus_device SENS_pol2 = pmbus_device(0x0D, "BMR461, CLK power supply", sensors);
//	pmbus_device SENS_pol3 = pmbus_device(0x0E, "BMR461, 3V3 power supply", sensors);
//	pmbus_device SENS_pol4 = pmbus_device(0x0F, "BMR461, 1V2 power supply", sensors);
//	pmbus_device SENS_PIM = pmbus_device(0x2B, "PIM4328PD, power input module", sensors);
//	pmbus_device SENS_DCDC = pmbus_device(0x2C, "BMR456 , DC/DC converter", sensors);
//	monitored_device SENS_temperature0 = monitored_device(0x4C, "TMP451, Temp. sensor node 0");
//	monitored_device SENS_temperature1 = monitored_device(0x4D, "TMP451, Temp. sensor node 1");
//	monitored_device SENS_temperature2 = monitored_device(0x4E, "TMP451, Temp. sensor node 2");
//	monitored_device SENS_temperature3 = monitored_device(0x4B, "TMP451, Temp. sensor node 3");
//	//reference all devices on the SENS node into a pointer array
//	static const uint8_t nof_sens_devices = 11; //input here the number of devices on the SENS node
//	monitored_device * SENS_devices[nof_sens_devices] = {
//						&SENS_pol0, &SENS_pol1, &SENS_pol2, &SENS_pol3, &SENS_pol4, &SENS_PIM, &SENS_DCDC,
//						&SENS_temperature0, &SENS_temperature1, &SENS_temperature2, &SENS_temperature3 };
//
//
//	// create all devices on an FPGA node on the uniboard
//	pmbus_device FPGA_NODE_pol0 = pmbus_device(0x01, "BMR464, Core supply", sensors);
//	pmbus_device FPGA_NODE_pol1 = pmbus_device(0x0D, "BMR461, VCC RAM", sensors);
//	pmbus_device FPGA_NODE_pol2 = pmbus_device(0x0E, "BMR461, Tranceiver power supply 0", sensors);
//	pmbus_device FPGA_NODE_pol3 = pmbus_device(0x0F, "BMR461, Tranceiver power supply 1", sensors);
//	pmbus_device FPGA_NODE_pol4 = pmbus_device(0x10, "BMR461, control power supply", sensors);
//	pmbus_device FPGA_NODE_pol5 = pmbus_device(0x11, "BMR461, FPGA IO power supply ", sensors);
//
//	//reference all devices on an FPGA_NODE into a pointer array
//	static const uint8_t nof_fpga_node_devices = 6;	//input here the number of devices on an FPGA_node
//	monitored_device *FPGA_NODE_devices[nof_fpga_node_devices] = { &FPGA_NODE_pol0, &FPGA_NODE_pol1, &FPGA_NODE_pol2,
//																   &FPGA_NODE_pol3, &FPGA_NODE_pol4, &FPGA_NODE_pol5 };
//
//
//	// create all nodes on an FPGA node(*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)
//	node unb_fpga_node_0 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_0", 0);
//	node unb_fpga_node_1 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_1", 1);
//	node unb_fpga_node_2 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_2", 2);
//	node unb_fpga_node_3 = node(FPGA_NODE_devices, nof_fpga_node_devices, nullptr, 0, nullptr, 0, "unb_fpga_node_3", 3);
//	node unb_sens_node = node(SENS_devices, nof_sens_devices, nullptr, 0, nullptr, 0, "unb_sens_node", 4);
//
//	//reference all subnodes in a pointer array
//	static const uint8_t nof_unb_subnodes = 5; //the number of subnodes a uniboard has
//	node * uniboard_subnodes[nof_unb_subnodes] = { &unb_fpga_node_0, &unb_fpga_node_1,
//												   &unb_fpga_node_2, &unb_fpga_node_3, &unb_sens_node };
//
//	//these switches are used by the uniboards to select the correct subnode channel
//	I2C_switch uniboard_A_switch = I2C_switch(0x70); //can have 0x70 to 0x77
//	I2C_switch uniboard_B_switch = I2C_switch(0x71); //can have 0x70 to 0x77
//	device * switch_A[1] = { &uniboard_A_switch };
//	device * switch_B[1] = { &uniboard_B_switch };
//
//	//create both uniboards (note: for a 2 unb APS it is possible to remove the uniboard here, or use one I2C unit less
//	node uniboard_A = node(nullptr, 0, uniboard_subnodes, nof_unb_subnodes, switch_A, 1, "uniboard A", NONE, 0);// , &uniboard_A_switch&);		//create uniboard 0
//	node uniboard_B = node(nullptr, 0, uniboard_subnodes, nof_unb_subnodes, switch_B, 1, "Uniboard B", NONE, 1);// , &uniboard_B_switch);		//create uniboard 1
//
//	//create a pointer array pointing to the uniboards
//	static const uint8_t nof_uniboards = 2;
//	node *subnodes[nof_uniboards]{ &uniboard_A,  &uniboard_B };
//
///*===================================================================================
//		base node configuration ends here
//===================================================================================*/
//
//public:
//	void update_buffer(float *databuff){
//		uint16_t buff_cnt = 0; //points to the element in the buffer array to update
//		read_node(I2C_Handle, buff_cnt, databuff);	//read the base node and all its associated devices and subnodes
//
//#if LOGGING_GENERAL == true
//		PRINTF( "updated: %d variables\r\n", buff_cnt);
//#endif
//	}
//
//	uint16_t nof_datapoints = 0;
//	//float *buffer; //create a new buffer that contains all the datapoints
//};
//
//class RCU_base_node : public node { //the base nodes that contain the RCU's (1 for RCU 0 - 15 and 1 for 16 - 31)
//public:
//	RCU_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "RCU2_base_node", uint8_t starting_rcu_num=0)
//		: node(nullptr, 0, subnodes, 2, nullptr, 0, _name, NONE, NONE){//, nullptr) {
//
//
//		//ensures the RCU's are named 0 to 31 correctly
//		//string insert_name;
//		for (int i = starting_rcu_num; i < starting_rcu_num + 16; i++) {
//			//PRINTF( i \r\n");
//
//			//sprintf(insert_name, "RCU_%02d", i);
//			//insert_name = "RCU_" + to_string(i);
//
//			if (i < 8 + starting_rcu_num) {
//				RCU_switch_subnodes_SW_0[i - starting_rcu_num]->name = "RCU_" + to_string(i);
//			}
//			else {
//				RCU_switch_subnodes_SW_1[i - 8 - starting_rcu_num]->name = "RCU_" + to_string(i);
//			}
//		}
//		I2C_Handle = _I2C_Handle;
//
//		nof_datapoints = get_nof_datapoints();
//		//buffer = new float[nof_datapoints]; //doesn't seem to be working in MCUXPRESSO, replaced with large enough array in 'device_manager.cpp'
//	}
//
//	lpi2c_rtos_handle_t *I2C_Handle;	//reference to the I2C unit used
//	uint16_t nof_datapoints = 0;		//counter (during initialisation) of how many monitored datapoints there are
//private:
//
///*===================================================================================
//		base node configuration starts here
//===================================================================================*/
//
//	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
//	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
//	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
//	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
//	//reference all sensors in a pointer array
//	static const uint8_t nof_sensors = 4;
//	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};
//
//
//	// create all devices on an RCU that are constantly monitored
//	pmbus_device generic_device_A = pmbus_device(0x65, "test_module 0x65", sensors);
//	pmbus_device generic_device_B = pmbus_device(0x4F, "test_module 0x4F", sensors);
//
//	//reference all devices on an FPGA_NODE into a pointer array
//	static const uint8_t nof_RCU_monitored = 2;	//input here the number of devices on an FPGA_node
//	monitored_device *RCU_monitored[nof_RCU_monitored] = { &generic_device_A, &generic_device_B };
//
//
//	IO_Expander IO_0 = IO_Expander(0x20, "TCA9539, IO_expander 0");
//	IO_Expander IO_1 = IO_Expander(0x21, "TCA9539, IO_expander 1");
//	IO_Expander IO_2 = IO_Expander(0x22, "TCA6416, IO_expander 2");
//
//	static const uint8_t nof_IO_expander_per_RCU = 3;
//	device  *IO_expanders[nof_IO_expander_per_RCU] = { &IO_0, &IO_1, &IO_2};
//
//	//create all the RCU's attached to a switch (*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)
//	node RCU_0 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 0);
//	node RCU_1 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 1);
//	node RCU_2 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 2);
//	node RCU_3 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 3);
//	node RCU_4 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 4);
//	node RCU_5 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 5);
//	node RCU_6 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 6);
//	node RCU_7 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 7);
//
//	node RCU_8 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 0);
//	node RCU_9 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 1);
//	node RCU_10 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 2);
//	node RCU_11 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 3);
//	node RCU_12 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 4);
//	node RCU_13 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 5);
//	node RCU_14 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 6);
//	node RCU_15 = node(RCU_monitored, nof_RCU_monitored, nullptr, 0, IO_expanders, nof_IO_expander_per_RCU, "RCU_", 7);
//
//	//reference all subnodes in a pointer array
//	static const uint8_t nof_RCU_switch_subnodes = 8; //each RCU switch has 8 RCU's attached to it
//	node * RCU_switch_subnodes_SW_0[nof_RCU_switch_subnodes] = { &RCU_0, &RCU_1, &RCU_2, &RCU_3,
//															&RCU_4, &RCU_5, &RCU_6, &RCU_7 };
//
//	node * RCU_switch_subnodes_SW_1[nof_RCU_switch_subnodes] = { &RCU_8, &RCU_9, &RCU_10, &RCU_11,
//															&RCU_12, &RCU_13, &RCU_14, &RCU_15 };
//
//
//	//these switches are used by the uniboards to select the correct subnode channel
//	I2C_switch RCU_switch_0 = I2C_switch(0x70, "I2C_switch_0"); //can have 0x70 to 0x77
//	I2C_switch RCU_switch_1 = I2C_switch(0x71, "I2C_switch_1");	//can have 0x70 to 0x77
//	device * switch_0[1] = { &RCU_switch_0 };
//	device * switch_1[1] = { &RCU_switch_1 };
//
//	// create the RCU switch nodes
//	node RCU_SW_node_0 = node(nullptr, 0, RCU_switch_subnodes_SW_0, nof_RCU_switch_subnodes, switch_0, 1, "RCU switch A", NONE, 0);// , &RCU_switch_0);
//	node RCU_SW_node_1 = node(nullptr, 0, RCU_switch_subnodes_SW_1, nof_RCU_switch_subnodes, switch_1, 1, "RCU switch B", NONE, 1);// , &RCU_switch_1);
//
//	//create a pointer array pointing to the RCU switches
//	static const uint8_t nof_switch_nodes = 2;
//	node *subnodes[nof_switch_nodes]{ &RCU_SW_node_0,  &RCU_SW_node_1 };
//
///*===================================================================================
//		base node configuration ends here
//===================================================================================*/
//
//public:
//	void update_buffer(float *databuff) {
//		uint16_t buff_cnt = 0; //points to the element in the buffer array to update
//		read_node(I2C_Handle, buff_cnt, databuff);
//#if LOGGING_GENERAL == true
//		PRINTF( "updated: %d variables \r\n", buff_cnt);
//#endif
//	}
////	void printout_buffer() {
////		PRINTF("nof datapoint to pritn: %d\r\n", nof_datapoints);
////		for (int i = 0; i < nof_datapoints; i++) {
////			PRINTF("%f \r\n", buffer[i] );
////		}
////	}
//
//	//allows you to set all IO expanders outputs to the same value
//	void set_expanders_all(uint16_t value[nof_IO_expander_per_RCU]) {
//
//		IO_Expander *expander;
//		uint8_t expander_cnt = 0;
//
//		//store the current configuration of the switches
//		uint8_t switches_config[] = { RCU_switch_0.current_config, RCU_switch_1.current_config };
//
//		//todo: broadcasting probably isn't safe, as there is no certainty all devices were reached succesfully.
//		RCU_switch_0.set_broadcast(I2C_Handle);
//		RCU_switch_1.set_broadcast(I2C_Handle);
//
//
//		for (int j = 0; j < nof_IO_expander_per_RCU; j++) {	//for all non-monitored devices in the RCU nodes
//			if (IO_expanders[j]->type == IO_expander_device) {	//make sure its an IO_expander
//				expander = reinterpret_cast<IO_Expander*>(IO_expanders[j]);	//interpret generic 'device' as IO_expander
////#if LOGGING_GENERAL  == true
//				PRINTF("%s \r\n", IO_expanders[j]->name.c_str());
////#endif
//				expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expanders
//				expander_cnt++;
//			}
//		}
//
//		//restore the previous configuration
//		RCU_switch_0.set_configuration(I2C_Handle, switches_config[0]);
//		RCU_switch_1.set_configuration(I2C_Handle, switches_config[1]);
//	}
//
//	void set_expander_single_node(string RCU_node_name, uint16_t value[nof_IO_expander_per_RCU]) {
//		IO_Expander *expander;
//		uint8_t expander_cnt = 0;
//
//		//remember how the IO switches were configured
//		uint8_t switches_config[2] = { RCU_switch_0.current_config, RCU_switch_1.current_config };
//		RCU_switch_0.set_off(I2C_Handle);	//turn off switch A
//		RCU_switch_1.set_off(I2C_Handle);	//turn off switch B
//
//		for (int k = 0; k < nof_switch_nodes; k++) {	//for all subnodes	 (the switch nodes, should be 2)
//
//			for (int i = 0; i < nof_RCU_switch_subnodes; i++) {	//for all the subnodes in the subnodes (the RCU nodes, should be 9)
//				PRINTF("%s\r\n", subnodes[k]->subnodes[i]->name.c_str());
//				if (subnodes[k]->subnodes[i]->name == RCU_node_name) {	//check if this is the correct node
//					subnodes[k]->switcher->set_channel(I2C_Handle, subnodes[k]->subnodes[i]->switch_channel); //set the switch to this channel
//
//					for (int j = 0; j < subnodes[k]->subnodes[i]->nof_devices; j++) {	//for all non-monitored devices in the RCU node
//						if (subnodes[k]->subnodes[i]->devices[j]->type == IO_expander_device) {	//make sure its an IO_expander
//
//							expander = reinterpret_cast<IO_Expander*>(subnodes[k]->subnodes[i]->devices[j]);	//interpret generic 'device' as IO_expander
//							expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expander
//							expander_cnt++;
//						}
//					}
//					subnodes[k]->switcher->set_off(I2C_Handle); //set to off after usage
//				}
//			}
//		}
//		RCU_switch_0.set_configuration(I2C_Handle, switches_config[0]);	//restore switch channel settings
//		RCU_switch_1.set_configuration(I2C_Handle, switches_config[1]);	//restore switch channel settings
//	}
//	//float *buffer =nullptr; //create a new buffer that contains all the datapoints
//
//
//	//float *buffer = new float[nof_datapoints]; //create a new buffer that contains all the datapoints
//
//};
//
//
////TODO: the Test_base_node, RCU_base_node and UNB_base_node should be derived from a more general "Base_node" class, which in turn is derived from the "node" class
//class TEST_base_node : public node { //base node during testing, specifically made to fit the available devices
//public:
//	TEST_base_node(lpi2c_rtos_handle_t *_I2C_Handle, string _name = "TEST_base_node")
//		: node(nullptr, 0, subnodes, 2, nullptr, 0, _name, NONE, NONE){//, nullptr) {
//
//
//		I2C_Handle = _I2C_Handle;
//
//		nof_datapoints = get_nof_datapoints();
//		//buffer = new float[nof_datapoints]; //doesn't seem to be working in MCUXPRESSO, replaced with large enough array in 'device_manager.cpp'
//	}
//
//	lpi2c_rtos_handle_t *I2C_Handle;	//reference to the I2C unit used
//	uint16_t nof_datapoints = 0;		//counter (during initialisation) of how many monitored datapoints there are
//private:
//
///*===================================================================================
//		base node configuration starts here
//===================================================================================*/
//
//	//all sensors of a pmbus device we're interested in
//	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
//	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
//	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
//	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
//	//reference all sensors in a pointer array
//	static const uint8_t nof_sensors = 4;
//	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};
//
//
//	// create all devices that are constantly monitored
//	pmbus_device BMR451_0 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
//	pmbus_device BMR462_1 = pmbus_device(0x65, "BMR462_0x65", sensors);
//	pmbus_device BMR451_2 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
//	pmbus_device BMR462_3 = pmbus_device(0x65, "BMR462_0x65", sensors);
//	pmbus_device BMR451_4 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
//	pmbus_device BMR462_5 = pmbus_device(0x65, "BMR462_0x65", sensors);
//	pmbus_device BMR451_6 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
//	pmbus_device BMR462_7 = pmbus_device(0x65, "BMR462_0x65", sensors);
////	pmbus_device BMR451_8 = pmbus_device(0x4F, "BMR451_0x4F", sensors);
////	pmbus_device BMR462_9 = pmbus_device(0x65, "BMR462_0x65", sensors);
////	pmbus_device BMR451_A = pmbus_device(0x4F, "BMR451_0x4F", sensors);
////	pmbus_device BMR462_B = pmbus_device(0x65, "BMR462_0x65", sensors);
////	pmbus_device BMR451_C = pmbus_device(0x4F, "BMR451_0x4F", sensors);
////	pmbus_device BMR462_D = pmbus_device(0x65, "BMR462_0x65", sensors);
////	pmbus_device BMR451_E = pmbus_device(0x4F, "BMR451_0x4F", sensors);
////	pmbus_device BMR462_F = pmbus_device(0x65, "BMR462_0x65", sensors);
//
//	//reference all devices on an FPGA_NODE into a pointer array
//	//static const uint8_t nof_monitored = 2;	//input here the number of devices on an FPGA_node
//	//monitored_device *monitored[nof_monitored] = { &BMR462, &BMR451 };
//	static const uint8_t nof_monitored = 8;	//input here the number of devices on an FPGA_node
//	monitored_device *monitored[nof_monitored] = { 	&BMR451_0, &BMR462_1, &BMR451_2, &BMR462_3,
//													&BMR451_4, &BMR462_5, &BMR451_6, &BMR462_7};
////													&BMR451_8, &BMR462_9, &BMR451_A, &BMR462_B,
////													&BMR451_C, &BMR462_D, &BMR451_E, &BMR462_F };
//
//	//non monitored devices
//	IO_Expander IO_0 = IO_Expander(0x73, "IO_expander 0");
//	IO_Expander IO_1 = IO_Expander(0x74, "IO_expander 1");
//	IO_Expander IO_2 = IO_Expander(0x75, "IO_expander 2");
//	//create poitner array of all non-monitored devices in a node
//	static const uint8_t nof_IO_expander = 3;
//	device  *IO_expanders[nof_IO_expander] = { &IO_0, &IO_1, &IO_2};
//
//	//create all the subnodes attached to a switch (*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)
//
//	//switch A
//	node subnode_A = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_A", 0);
//	node subnode_B = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_B", 1);
//	node subnode_C = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_C", 2);
//	node subnode_D = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_D", 3);
//
//	//switch B
//	node subnode_E = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_E", 0);
//	node subnode_F = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_F", 1);
//	node subnode_G = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_G", 2);
//	node subnode_H = node(monitored, nof_monitored, nullptr, 0, IO_expanders, nof_IO_expander, "device_node_H", 3);
//
//	//reference all subnodes in a pointer array
//	static const uint8_t nof_switch_subnodes = 4; //each switch has 4 subnodes attached to it
//	node * switch_subnodes_SW_0[nof_switch_subnodes] = { &subnode_A, &subnode_B, &subnode_C, &subnode_D};
//	node * switch_subnodes_SW_1[nof_switch_subnodes] = { &subnode_E, &subnode_F, &subnode_G, &subnode_H};
//
//
//	//these switches are used to select the correct subnode channel
//	I2C_switch switch_0 = I2C_switch(0x70, "I2C_switch_0"); //can have 0x70 to 0x77
//	I2C_switch switch_1 = I2C_switch(0x70, "I2C_switch_1");	//can have 0x70 to 0x77
//	device * switch_0_ptr[1] = { &switch_0 };
//	device * switch_1_ptr[1] = { &switch_0 };
//
//	// create the RCU switch nodes
//	node subnode_0 = node(nullptr, 0, switch_subnodes_SW_0, nof_switch_subnodes, switch_0_ptr, 1, "switch node A", NONE, 0);
//	node subnode_1 = node(nullptr, 0, switch_subnodes_SW_1, nof_switch_subnodes, switch_1_ptr, 1, "switch node B", NONE, 1);
//
//	//create a pointer array pointing to the switch subnodes
//	static const uint8_t nof_switch_nodes = 2;
//	node *subnodes[nof_switch_nodes]{ &subnode_0,  &subnode_1 };
//
///*===================================================================================
//		base node configuration ends here
//===================================================================================*/
//
//public:
//	void update_buffer(float *databuff) {
//		uint16_t buff_cnt = 0; //points to the element in the buffer array to update
//		uint16_t failures = read_node(I2C_Handle, buff_cnt, databuff);
//		PRINTF("unable to read %d devices \r\n\r\n", failures);
//	}
//
//
//	//allows you to set all IO expanders outputs to the same value
//	void set_expanders_all(uint16_t value[nof_IO_expander]) {
//
//		IO_Expander *expander;
//		uint8_t expander_cnt = 0;
//
//		//store the current configuration of the switches (though is likely to already be off)
//		uint8_t switches_config[] = { switch_0.current_config, switch_1.current_config };
//
//		//todo: broadcasting probably isn't safe, as there is no certainty all devices were reached succesfully.
//		switch_0.set_broadcast(I2C_Handle);
//		switch_1.set_broadcast(I2C_Handle);
//
//
//		for (int j = 0; j < nof_IO_expander; j++) {	//for all non-monitored devices in the RCU nodes
//			if (IO_expanders[j]->type == IO_expander_device) {	//make sure its an IO_expander
//				expander = reinterpret_cast<IO_Expander*>(IO_expanders[j]);	//interpret generic 'device' as IO_expander
////#if LOGGING_GENERAL  == true
//				PRINTF("%s \r\n", IO_expanders[j]->name.c_str());
////#endif
//				expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expanders
//				expander_cnt++;
//			}
//		}
//
//		//restore the previous configuration
//		switch_0.set_configuration(I2C_Handle, switches_config[0]);
//		switch_1.set_configuration(I2C_Handle, switches_config[1]);
//	}
//
//	void set_expander_single_node(string RCU_node_name, uint16_t value[nof_IO_expander]) {
//		IO_Expander *expander;
//		uint8_t expander_cnt = 0;
//
//		//remember how the IO switches were configured
//		uint8_t switches_config[2] = { switch_0.current_config, switch_1.current_config };
//		switch_0.set_off(I2C_Handle);	//turn off switch A
//		switch_1.set_off(I2C_Handle);	//turn off switch B
//
//		for (int k = 0; k < nof_switch_nodes; k++) {	//for all subnodes	 (the switch nodes, should be 2)
//
//			for (int i = 0; i < nof_switch_subnodes; i++) {	//for all the subnodes in the subnodes (should be 4)
//
//				if (subnodes[k]->subnodes[i]->name == RCU_node_name) {	//check if this is the correct node
//					subnodes[k]->switcher->set_channel(I2C_Handle, subnodes[k]->subnodes[i]->switch_channel); //set the switch to this channel
//
//					for (int j = 0; j < subnodes[k]->subnodes[i]->nof_devices; j++) {	//for all non-monitored devices in the RCU node
//						if (subnodes[k]->subnodes[i]->devices[j]->type == IO_expander_device) {	//make sure its an IO_expander
//
//							expander = reinterpret_cast<IO_Expander*>(subnodes[k]->subnodes[i]->devices[j]);	//interpret generic 'device' as IO_expander
//							expander->set_output_values(I2C_Handle, value[expander_cnt]);	//set the correct values to the IO_expander
//							expander_cnt++;
//						}
//					}
//					subnodes[k]->switcher->set_off(I2C_Handle); //set to off after usage
//				}
//			}
//		}
//		switch_0.set_configuration(I2C_Handle, switches_config[0]);	//restore switch channel settings
//		switch_1.set_configuration(I2C_Handle, switches_config[1]);	//restore switch channel settings
//	}
//	//float *buffer =nullptr; //create a new buffer that contains all the datapoints
//
//
//	//float *buffer = new float[nof_datapoints]; //create a new buffer that contains all the datapoints
//
//};
//
//
//#endif /* CLASSES_H_ */
