################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/src/netif/ppp/polarssl/arc4.c \
../lwip/src/netif/ppp/polarssl/des.c \
../lwip/src/netif/ppp/polarssl/md4.c \
../lwip/src/netif/ppp/polarssl/md5.c \
../lwip/src/netif/ppp/polarssl/sha1.c 

OBJS += \
./lwip/src/netif/ppp/polarssl/arc4.o \
./lwip/src/netif/ppp/polarssl/des.o \
./lwip/src/netif/ppp/polarssl/md4.o \
./lwip/src/netif/ppp/polarssl/md5.o \
./lwip/src/netif/ppp/polarssl/sha1.o 

C_DEPS += \
./lwip/src/netif/ppp/polarssl/arc4.d \
./lwip/src/netif/ppp/polarssl/des.d \
./lwip/src/netif/ppp/polarssl/md4.d \
./lwip/src/netif/ppp/polarssl/md5.d \
./lwip/src/netif/ppp/polarssl/sha1.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/src/netif/ppp/polarssl/%.o: ../lwip/src/netif/ppp/polarssl/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu11 -DCPU_MIMXRT1062DVL6A -DUA_ARCHITECTURE_FREERTOSLWIP -DUA_ARCH_FREERTOS_USE_OWN_MEMORY_FUNCTIONS -DOPEN62541_FEERTOS_USE_OWN_MEM -DCPU_MIMXRT1062DVL6A_cm7 -DFSL_FEATURE_PHYKSZ8081_USE_RMII50M_MODE -D_POSIX_SOURCE -DSDK_DEBUGCONSOLE=0 -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DUSE_RTOS=1 -DPRINTF_ADVANCED_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DFSL_RTOS_FREE_RTOS -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__NEWLIB__ -I"D:\projects\MCUXpresso\example_MC\board" -I"D:\projects\MCUXpresso\example_MC\source" -I"D:\projects\MCUXpresso\example_MC" -I"D:\projects\MCUXpresso\example_MC\drivers" -I"D:\projects\MCUXpresso\example_MC\device" -I"D:\projects\MCUXpresso\example_MC\CMSIS" -I"D:\projects\MCUXpresso\example_MC\lwip\contrib\apps\ping" -I"D:\projects\MCUXpresso\example_MC\lwip\port\arch" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\compat\posix\arpa" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\compat\posix\net" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\compat\posix" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\compat\posix\sys" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\compat\stdc" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\lwip" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\lwip\priv" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\lwip\prot" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\netif" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\netif\ppp" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include\netif\ppp\polarssl" -I"D:\projects\MCUXpresso\example_MC\lwip\port" -I"D:\projects\MCUXpresso\example_MC\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\example_MC\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\example_MC\utilities" -I"D:\projects\MCUXpresso\example_MC\component\serial_manager" -I"D:\projects\MCUXpresso\example_MC\component\lists" -I"D:\projects\MCUXpresso\example_MC\component\uart" -I"D:\projects\MCUXpresso\example_MC\xip" -I"D:\projects\MCUXpresso\example_MC\lwip\src" -I"D:\projects\MCUXpresso\example_MC\lwip\src\include" -O0 -fno-common -g3 -Wall -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin -fmerge-constants -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


