/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*******************************************************************************
 * Includes
 ******************************************************************************/
///* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"


#include "ping.h"
#include "lwip/contrib/apps/tcpecho/tcpecho.h"
#include "open62541.h"

#include "lwip/opt.h"

#include "lwip/netifapi.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "lwip/prot/dhcp.h"
#include "lwip/sys.h"
#include "lwip/ip_addr.h"

#include "enet_ethernetif.h"
#include "board.h"



//#include "fsl_device_registers.h"
//#include "fsl_debug_console.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
#include "fsl_lpi2c.h"
#include "fsl_iomuxc.h"

#include "I2C_functions.h"

#include <math.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define BMR464_addr 0x4F
#define BMR462_addr 0x65
uint8_t adressed_device = BMR464_addr;


/* MAC address configuration. */
#define configMAC_ADDR                     \
    {                                      \
        0x02, 0x12, 0x13, 0x10, 0x15, 0x11 \
    }

/* Address of PHY interface. */
#define EXAMPLE_PHY_ADDRESS BOARD_ENET0_PHY_ADDRESS

/* System clock name. */
#define EXAMPLE_CLOCK_NAME kCLOCK_CoreSysClk


#ifndef EXAMPLE_NETIF_INIT_FN
/*! @brief Network interface initialization function. */
#define EXAMPLE_NETIF_INIT_FN ethernetif0_init
#endif /* EXAMPLE_NETIF_INIT_FN */

/*! @brief Priority of the temporary lwIP initialization thread. */
#define INIT_THREAD_PRIO DEFAULT_THREAD_PRIO


#define TICK_PERIOD_MS 		portTICK_PERIOD_MS	//how many ms there are in a tick (note: unsigned int, beware low number)

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
static void opcua_thread(void *arg);	//initialises server and runs it
static void server_manager(void *arg);	//fills and manages the servers address space (E.g. creates and updates variables)
float sensor_read();					//reads one on the sensors, serves as example
static UA_StatusCode	 helloWorldMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output);
static void addHellWorldMethod(UA_Server *server);
static UA_StatusCode switch_device_method(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output);
static void add_switch_device_method(UA_Server *server);
/*******************************************************************************
 * Variables
 ******************************************************************************/

/*******************************************************************************
 * Code
 ******************************************************************************/
void BOARD_InitModuleClock(void)
{
    const clock_enet_pll_config_t config = {.enableClkOutput = true, .enableClkOutput25M = false, .loopDivider = 1};
    CLOCK_InitEnetPll(&config);
}

void delay(void)
{
    volatile uint32_t i = 0;
    for (i = 0; i < 1000000; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

/*!
 * @brief Initializes lwIP stack.
 */
static void stack_init(void *arg)
{
	static struct netif netif;
	ip4_addr_t netif_ipaddr, netif_netmask, netif_gw;
	ethernetif_config_t enet_config = {
		.phyAddress = EXAMPLE_PHY_ADDRESS,
		.clockName  = EXAMPLE_CLOCK_NAME,
		.macAddress = configMAC_ADDR,
	};

	IP4_ADDR(&netif_ipaddr, 192, 168, 137, 102);
	IP4_ADDR(&netif_netmask, 255, 255, 255, 0);
	IP4_ADDR(&netif_gw, 192, 168, 137, 1);

	tcpip_init(NULL, NULL);

	delay();

	netif_add(&netif, &netif_ipaddr, &netif_netmask, &netif_gw, &enet_config, ethernetif0_init, tcpip_input);
	netif_set_default(&netif);
	netif_set_up(&netif);


	printf("ETH MAC addr: %X:%X:%X:%X:%X:%X\r\n", enet_config.macAddress[0],enet_config.macAddress[1],enet_config.macAddress[2],enet_config.macAddress[3],enet_config.macAddress[4],enet_config.macAddress[5]);
	printf("\r\n************************************************\r\n");
	printf(" PING example\r\n");
	printf("************************************************\r\n");
	printf(" IPv4 Address     : %u.%u.%u.%u\r\n", ((u8_t *)&netif_ipaddr)[0], ((u8_t *)&netif_ipaddr)[1],
		   ((u8_t *)&netif_ipaddr)[2], ((u8_t *)&netif_ipaddr)[3]);
	PRINTF(" IPv4 Subnet mask : %u.%u.%u.%u\r\n", ((u8_t *)&netif_netmask)[0], ((u8_t *)&netif_netmask)[1],
		   ((u8_t *)&netif_netmask)[2], ((u8_t *)&netif_netmask)[3]);
	PRINTF(" IPv4 Gateway     : %u.%u.%u.%u\r\n", ((u8_t *)&netif_gw)[0], ((u8_t *)&netif_gw)[1],
		   ((u8_t *)&netif_gw)[2], ((u8_t *)&netif_gw)[3]);
	PRINTF("************************************************\r\n");


	//start the OPC ua server
	if(NULL == sys_thread_new("opcua_thread", opcua_thread, NULL, 20000, 13)){
		LWIP_ASSERT("opcua(): Task creation failed.", 0);
	}

    vTaskDelete(NULL);
}

/*!
 * @brief Main function
 */
int main(void)
{
    gpio_pin_config_t gpio_config = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};

    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    BOARD_InitModuleClock();
    BOARD_InitBootPeripherals();	//initialise all peripherals (in this case only I2C)

    IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET1TxClkOutputDir, true);

    GPIO_PinInit(GPIO1, 9, &gpio_config);
    GPIO_PinInit(GPIO1, 10, &gpio_config);
    /* pull up the ENET_INT before RESET. */
    GPIO_WritePinOutput(GPIO1, 10, 1);
    GPIO_WritePinOutput(GPIO1, 9, 0);
    delay(); //delay();
    GPIO_WritePinOutput(GPIO1, 9, 1);


    NVIC_SetPriority(LPI2C3_IRQn, 2); //reduces I2Cc priority

    //initialise LWIP tcp stack
	if (sys_thread_new("stack_init", stack_init, NULL, 512, 7) == NULL){
		LWIP_ASSERT("stack_init(): Task creation failed.", 0);
	}

    vTaskStartScheduler();

    return 0;	/* Will not get here unless a task calls vTaskEndScheduler ()*/
}


static void opcua_thread(void *arg){
		UA_StatusCode retval;

        UA_UInt16 portNumber = 4840;

        UA_Server* mUaServer = UA_Server_new();
        UA_ServerConfig *uaServerConfig = UA_Server_getConfig(mUaServer);
        UA_ServerConfig_setMinimal(uaServerConfig, portNumber, 0);

        //set custom buffers, since default 64k is pretty high
        uaServerConfig->networkLayers->localConnectionConfig.recvBufferSize = 16384;
        uaServerConfig->networkLayers->localConnectionConfig.sendBufferSize = 16384;
        uaServerConfig->networkLayers->localConnectionConfig.localMaxMessageSize = 16384;

        //VERY IMPORTANT: Set the hostname with your IP before starting the server
        UA_ServerConfig_setCustomHostname(uaServerConfig, UA_STRING("192.168.137.102"));

        //fill and manage the servers address space
    	if (sys_thread_new("server_manager", server_manager, mUaServer, 1024, 7) == NULL){
    		LWIP_ASSERT("server_manager(): Task creation failed.", 0);
    	}

        UA_Boolean running = true;	//if set to false will stop the server"

        retval = UA_Server_run(mUaServer, &running);
        UA_Server_delete(mUaServer);

        if(retval != 0){
        	PRINTF("Server shut down unexpectedly\r\n");
        }



        vTaskDelete(NULL);
}


static void server_manager(void *arg){
	UA_Server* mUaServer = arg;

	addHellWorldMethod(mUaServer);
	add_switch_device_method(mUaServer);


    // add a variable node to the adresspace
    UA_VariableAttributes attr = UA_VariableAttributes_default;
    UA_Int32 myInteger = 42;
    UA_Variant_setScalarCopy(&attr.value, &myInteger, &UA_TYPES[UA_TYPES_INT32]);
    attr.description = UA_LOCALIZEDTEXT_ALLOC("en-US","sensors");
    attr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US","sensors");

    /* Add the variable node to the information model */
    UA_NodeId TestNodeId = UA_NODEID_STRING_ALLOC(1, "sensors");
    UA_QualifiedName myIntegerName = UA_QUALIFIEDNAME_ALLOC(1, "sensors");
    UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
    UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(mUaServer, TestNodeId, parentNodeId,
                                                            parentReferenceNodeId, myIntegerName,
                                                            UA_NODEID_NULL, attr, NULL, NULL);


    // add a variable node to the adresspace
    UA_VariableAttributes attr1 = UA_VariableAttributes_default;
    UA_Float myFloat = INFINITY;
    UA_Variant_setScalarCopy(&attr1.value, &myFloat, &UA_TYPES[UA_TYPES_FLOAT]);
    attr1.description = UA_LOCALIZEDTEXT_ALLOC("en-US","BM464_temperature");
    attr1.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US","BM464_temperature");
    /* Add the variable node to the information model */
    UA_NodeId nodeID1 = UA_NODEID_STRING_ALLOC(1, "BM464_temperature");
    UA_QualifiedName nodeName1 = UA_QUALIFIEDNAME_ALLOC(1, "BM464_temperature");
    UA_NodeId parentReferenceNodeId1 = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);
    UA_Server_addVariableNode(mUaServer, nodeID1, TestNodeId,
                                                            parentReferenceNodeId1, nodeName1,
                                                            UA_NODEID_NULL, attr1, NULL, NULL);

	/* allocations on the heap need to be freed */
	UA_VariableAttributes_clear(&attr);
	UA_NodeId_clear(&TestNodeId);
	UA_QualifiedName_clear(&myIntegerName);

	/* allocations on the heap need to be freed */
	UA_VariableAttributes_clear(&attr1);
	//UA_NodeId_clear(&nodeID1);
	UA_QualifiedName_clear(&nodeName1);



	while(1){
		myFloat = sensor_read();
	    UA_Variant value1;
	    UA_Variant_setScalar(&value1, &myFloat, &UA_TYPES[UA_TYPES_FLOAT]);
	    UA_Server_writeValue(mUaServer, nodeID1, value1);

	    vTaskDelay(pdMS_TO_TICKS(1000));
	}
}


float sensor_read(){	//

	status_t status;
	float output = 0;
    uint16_t raw_word = 0;

	status = I2C_read_word(&LPI2C3_masterHandle, adressed_device, 0x8D, &raw_word);

	if(status == kStatus_Success){
		printf("%s: received: \t%hX\r\n", pcTaskGetName(NULL), raw_word);

		//lines of code extracted from pmbus_sensor::format_literal function


		uint16_t mantissa = raw_word & 0b0000011111111111;  // mask upper 5 bits to 0
		uint16_t exponent = raw_word >> 11;  // shift exponent to the rightmost bit

		if ((exponent & 0b10000) >= 1) {  // if the exponent is negative(MSB == 1)
			exponent = exponent & 0b01111;
			exponent = exponent - 16;
		}
		if ((mantissa & 0b10000000000) >= 1) {  // if the mantissa is negative(MSB == 1)
			mantissa = mantissa & 0b01111111111;
			mantissa = mantissa - 1024;

		}

		output = pow(2, (int8_t)(exponent)) * (float)(mantissa);  // value = mantissa * 2 ^ exponent

		printf("%s: formatted: %f\r\n\r\n", pcTaskGetName(NULL), output);
	}
	else if(status == kStatus_LPI2C_Busy){
		//the busy flag seems to be triggered when handling the wires carrying the signal while running
		//the busy error does not seem to go away once triggered. TODO: maybe add reset whenever this happens
		PRINTF("%s: I2C unit is busy \r\n", pcTaskGetName(NULL));
		return INFINITY;
	}
	else {
		//all other errors here
		PRINTF("%s: I2C error occured \r\n", pcTaskGetName(NULL));
		return INFINITY;
	}
	return output;
}

static UA_StatusCode
helloWorldMethodCallback(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {
    UA_String *inputStr = (UA_String*)input->data;
    UA_String tmp = UA_STRING_ALLOC("Hello ");
    if(inputStr->length > 0) {
        tmp.data = (UA_Byte *)UA_realloc(tmp.data, tmp.length + inputStr->length);
        memcpy(&tmp.data[tmp.length], inputStr->data, inputStr->length);
        tmp.length += inputStr->length;
    }
    UA_Variant_setScalarCopy(output, &tmp, &UA_TYPES[UA_TYPES_STRING]);
    UA_String_clear(&tmp);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Hello World was called");
    return UA_STATUSCODE_GOOD;
}


static void addHellWorldMethod(UA_Server *server) {
    UA_Argument inputArgument;
    UA_Argument_init(&inputArgument);
    inputArgument.description = UA_LOCALIZEDTEXT("en-US", "A String");
    inputArgument.name = UA_STRING("MyInput");
    inputArgument.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    inputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument;
    UA_Argument_init(&outputArgument);
    outputArgument.description = UA_LOCALIZEDTEXT("en-US", "A String");
    outputArgument.name = UA_STRING("MyOutput");
    outputArgument.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    outputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes helloAttr = UA_MethodAttributes_default;
    helloAttr.description = UA_LOCALIZEDTEXT("en-US","Say `Hello World`");
    helloAttr.displayName = UA_LOCALIZEDTEXT("en-US","Hello World");
    helloAttr.executable = true;
    helloAttr.userExecutable = true;
    UA_Server_addMethodNode(server, UA_NODEID_NUMERIC(1,62541),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(1, "hello world"),
                            helloAttr, &helloWorldMethodCallback,
                            1, &inputArgument, 1, &outputArgument, NULL, NULL);
}

static UA_StatusCode switch_device_method(UA_Server *server,
                         const UA_NodeId *sessionId, void *sessionHandle,
                         const UA_NodeId *methodId, void *methodContext,
                         const UA_NodeId *objectId, void *objectContext,
                         size_t inputSize, const UA_Variant *input,
                         size_t outputSize, UA_Variant *output) {

	UA_Byte  addr = *(UA_Byte*)input->data;

    UA_Variant_setScalarCopy(output, &addr, &UA_TYPES[UA_TYPES_BYTE]);
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "setting sensor read I2C address to %d", addr);
    adressed_device = addr;

    return UA_STATUSCODE_GOOD;
}

static void add_switch_device_method(UA_Server *server) {
    UA_Argument inputArgument;
    UA_Argument_init(&inputArgument);
    inputArgument.description = UA_LOCALIZEDTEXT("en-US", "device address");
    inputArgument.name = UA_STRING("address_in");
    inputArgument.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
    inputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_Argument outputArgument;
    UA_Argument_init(&outputArgument);
    outputArgument.description = UA_LOCALIZEDTEXT("en-US", "device address");
    outputArgument.name = UA_STRING("address_out");
    outputArgument.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
    outputArgument.valueRank = UA_VALUERANK_SCALAR;

    UA_MethodAttributes infoAttr = UA_MethodAttributes_default;
    infoAttr.description = UA_LOCALIZEDTEXT("en-US","Say `set address`");
    infoAttr.displayName = UA_LOCALIZEDTEXT("en-US","set address");
    infoAttr.executable = true;
    infoAttr.userExecutable = true;
    UA_Server_addMethodNode(server, UA_NODEID_NUMERIC(1,625422),	// when initiating many things, maybe use incrementing counter
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_HASORDEREDCOMPONENT),
                            UA_QUALIFIEDNAME(1, "set_address"),
							infoAttr, &switch_device_method,
                            1, &inputArgument, 1, &outputArgument, NULL, NULL);
}

void vApplicationMallocFailedHook(){
	PRINTF("Oh boy, time to die! (Malloc fail)\r\n");
	for(;;){
			vTaskDelay(pdMS_TO_TICKS(1000));
	}
}

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName ){
	PRINTF("Oh boy, time to die! (stack overflow)\r\n");
	for(;;){
			vTaskDelay(pdMS_TO_TICKS(1000));
	}
}



